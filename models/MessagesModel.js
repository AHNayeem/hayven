module.exports = {
    fields:{
        msg_id: {
            type: "timeuuid",
            default: {"$db_function": "now()"}
        },
        created_at: {
            type: "timestamp",
            default: {"$db_function": "toTimestamp(now())"}
        },
        conversation_id: {type: "uuid"},
        sender: {type: "uuid"},
        sender_name: "text",
        sender_img: "text",
        msg_body: "text",
        call_duration: "text",
        has_flagged: { type: "set", typeDef: "<text>"},
        msg_type: { type: "text", default: "text" },
        msg_status: {type: "set", typeDef: "<text>"},
        attch_imgfile: { type: "set", typeDef: "<text>" },
        attch_audiofile: { type: "set", typeDef: "<text>" },
        attch_videofile: { type: "set", typeDef: "<text>" },
        attch_otherfile: { type: "set", typeDef: "<text>" },
        has_delete: { type: "set", typeDef: "<text>" },
        has_emoji: { type: "map", typeDef: "<text, int>", default: {"grinning": 0,
                                                                  "joy": 0, 
                                                                  "open_mouth": 0, 
                                                                  "disappointed_relieved": 0, 
                                                                  "rage": 0, 
                                                                  "thumbsup": 0, 
                                                                  "thumbsdown": 0, 
                                                                  "heart": 0}},
        has_tag_text: { type: "set", typeDef: "<text>" },
        has_reply: {type: "int", default: 0},
        last_reply_time: {type: "timestamp"},
        url_favicon: {type: "text", default: null},
        url_base_title: {type: "text", default: null},
        url_title: {type: "text", default: null},
        url_body: {type: "text", default: null},
        url_image: {type: "text", default: null}
    },
    // key:["conversation_id", "msg_id", "created_at"],
    // clustering_order: {"created_at": "desc"}
    key:["msg_id"],
    indexes: ["conversation_id", "sender", "msg_status"]
}