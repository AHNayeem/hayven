module.exports = {
    fields:{
        tag_id: {
            type: "timeuuid",
            default: {"$db_function": "timeuuid()"}
        },
        created_by: {type: "uuid"},
        created_at: {
            type: "timestamp",
            default: {"$db_function": "toTimestamp(now())"}
        },
        tag_title:  {type: "text", default: "na"},
        tagged_by: {type: "uuid"},
        conversation_id: {type: "uuid"},
        message_id: {type: "uuid"}
    },
    key:["tag_id"],
    indexes: ["conversation_id", "message_id", "created_by"]
}