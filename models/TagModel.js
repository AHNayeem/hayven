module.exports = {
	fields:{
        tag_id: {
            type: "uuid",
            default: {"$db_function": "uuid()"}
        },
        tagged_by: {type: "uuid"},
        conversation_id: {type: "uuid"},
        title: "text",
        created_at: {
            type: "timestamp",
            default: {"$db_function": "toTimestamp(now())"}
        }
    },
    key:["tag_id"],
    indexes: ["conversation_id", "title"]
}
