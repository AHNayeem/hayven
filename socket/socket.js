var _ = require('lodash');

module.exports = function(io) {
  var app = require('express');
  var router = app.Router();
  var {
        generateMessage,
        sendNewMsg,
        sendCallMsg,
        sendBusyMsg,
        getUserIsBusy,
        hasMessageThisTag,
        deleteThisTag,
        replyId,
        thread_reply_update,
        find_reply_list,
        url_preview,
        readOldMessage
      } = require('./../utils/message');
  var {
        saveConversation,
        findConversationHistory,
        createGroup,
        createPersonalConv,
        findConvDetail,
        saveNewGroup,
        updateGroupName,
        updateKeySpace,
        updatePrivecy,
        updateRoomimg,
        saveTag,
        findtag
      } = require('./../utils/conversation');

  var {file_unlink} = require('./../utils/files');

  var {hayvenjs} = require('./../utils/hayvenjs');

  io.on('connection', function(socket){
    socket.join('1');
    // socket.join('bde4b452-0734-4aaf-b5c1-09cc12d7ab64');
    socket.on('login', function(userdata){
      socket.join('navigate_2018_902770');
      socket.join(userdata.from);

      // if(typeof userdata.room_groups !== 'undefined'){
      //   _.forEach(userdata.room_groups, function(v, k){
      //     socket.join(v.conversation_id);
      //   });
      // }

      socket.handshake.session.userdata = userdata;
      socket.handshake.session.save();

      if(alluserlist.indexOf(userdata.from) != -1){
        console.log("socket js line 13");
      }else{
        alluserlist.push(userdata.from);
      }
      // console.log(alluserlist);
      // var room = io.sockets.adapter.rooms['navigate_2018_902770'];
      // room.length
      socket.emit('online_user_list', generateMessage('Admin', alluserlist)); // this emit receive only who is login
      socket.broadcast.emit('new_user_notification', generateMessage('Admin', socket.handshake.session.userdata)); // this emit receive all users except me
    });

    socket.on('disconnect', function(){
      io.sockets.in('navigate_2018_902770').emit('logout', { userdata: socket.handshake.session.userdata });
      if(socket.handshake.session.userdata){
        alluserlist = alluserlist.filter(function(el){
          return el !== socket.handshake.session.userdata.from;
        });
        delete socket.handshake.session.userdata;
        socket.handshake.session.save();
      }
    });

    socket.on('o2otoGroup', function(message, callback) {
      if(message.currentConvID != ""){
        findConvDetail(message.currentConvID,(result,err)=>{
            if(err){ throw err;}
            else{
              console.log(result);
              var previousList = result.conversationDetail[0].participants;
              var newMember = [message.targetUserID];
              var conversationMemList = previousList.concat(newMember);
              saveNewGroup(conversationMemList,message.ecosystem,message.crtUserID.toString(),(result,err)=>{
                if(err){ throw err;}
                else{
                  callback(result);
                }
              });
            }
        });
      }
    });

    socket.on('saveGroupName', function(message,callback){
      if(message.currentConvID != ""){
        updateGroupName(message.conversation_id,message.newGroupname,(result,err)=>{
          if(err){
            throw err;
          }else{
            callback(result);
          }
        });
      }
    });


    socket.on('sendMessage', function(message, callback) {
      if(message.is_room === false){
        sendNewMsg( socket.handshake.session.userdata.from,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(72, err);
          } else {
            if(result.status) {
              if(socket.handshake.session.userdata.from.toString() !== message.to.toString()){
                io.to(message.to).emit('newMessage', {
                  msg_id: result.res,
                  msg_from: socket.handshake.session.userdata.from,
                  msg_text: message.text,
                  msg_sender_name: message.sender_name,
                  msg_sender_img: message.sender_img,
                  msg_attach_files: message.attach_files,
                  msg_conv_id: message.conversation_id,
                  msg_thread_root_id: message.thread_root_id
                });
              }
              // console.log('socketjs 38', result);
              callback(result);
            } else {
              console.log(result);
            }
          }
        });
      }
      else if(message.is_room === true) {
        // console.log('something wrong!!!');
        // io.to('navigate_2018_902770').emit('newMessage', generateMessage(socket.handshake.session.userdata.from, message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img));
        // This is temporary group message.
        sendNewMsg( socket.handshake.session.userdata.from,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(err);
          } else {
            if(result.status) {
              socket.broadcast.emit('newMessage', {
                msg_id: result.res,
                msg_from: message.to,
                msg_text: message.text,
                msg_sender_name: message.sender_name,
                msg_sender_img: message.sender_img,
                msg_attach_files: message.attach_files,
                msg_conv_id: message.conversation_id,
                msg_thread_root_id: message.thread_root_id
              }); // this emit receive all users except me
              // io.to(message.conversation_id).emit('newMessage', {msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id}); // this emit receive all users except me
              // console.log({msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id});
              callback(result);
            } else {
              console.log(result);
            }
          }
        });
      }
    });

    /*for mobile*/
    socket.on('sendMessageFromMobile', function(message, callback) {
      console.log(119, message);
      if(message.is_room === false){
        sendNewMsg( message.sender_id,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(127, err);
          } else {
            if(result.status) {
              io.to(message.to).emit('newMessage', {
                        msg_id: result.res,
                        msg_from: message.sender_id,
                        msg_text: message.text,
                        msg_sender_name: message.sender_name,
                        msg_sender_img: message.sender_img,
                        msg_attach_files: message.attach_files,
                        msg_conv_id: message.conversation_id,
                        msg_thread_root_id: message.thread_root_id
                      });
              // console.log('socketjs 38', result);
              callback(result);
            } else {
              console.log(result);
            }
          }
        });
      }
      else if(message.is_room === true) {
        // console.log('something wrong!!!');
        // io.to('navigate_2018_902770').emit('newMessage', generateMessage(socket.handshake.session.userdata.from, message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img));
        // This is temporary group message.
        sendNewMsg( message.sender_id,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(err);
          } else {
            if(result.status) {
              socket.broadcast.emit('newMessage', {
                msg_id: result.res,
                msg_from: message.to,
                msg_text: message.text,
                msg_sender_name: message.sender_name,
                msg_sender_img: message.sender_img,
                msg_attach_files: message.attach_files,
                msg_conv_id: message.conversation_id }); // this emit receive all users except me
              // io.to(message.conversation_id).emit('newMessage', {msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id}); // this emit receive all users except me
              // console.log({msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id});
              callback(result);
            } else {
              console.log(result);
            }
          }
        });
      }
    });

    socket.on('msg_url2preview', function(data, callback){
      url_preview(data, (res) =>{
        if(res.status){
          socket.broadcast.emit('url2preview', res.body);
          callback(res.body);
        }else{
          console.log('socket 160', res);
        }
      });
    });

    socket.on('unlink_file', function(data){
      file_unlink(data);
    });

    socket.on('seen_emit', function(data){
      io.to(data.senderid).emit('receive_emit', data);
    });

    socket.on('update_msg_seen', function(data){
      io.to(data.senderid).emit('update_msg_receive_seen', data);
    });

    socket.on('one_user_deleted_this', function(data){
      socket.broadcast.emit('delete_from_all', data);
    });

    socket.on('update_thread_count', function(data) {
      thread_reply_update(data.msg_id, (result) =>{
        if(result.status){
            console.log('socket update_thread_count update status = ', result);
            socket.broadcast.emit('update_thread_counter', data);
          // callback(_.toString(result.result));
        } else {
          console.log('socket 130', result);
          // callback(result);
        }
      });
    });

    socket.on('find_reply', function(data, callback){
      find_reply_list(data.msg_id, data.conversation_id, (result) =>{
        callback(result);
      });
    });

  	socket.on('addNewMeberToGroup',function(message, callback){
  	  io.to(message.userID).emit('popUpgroupblock', {
  		senderName: message.senderName,
  		userID: message.userID,
  		userName: message.userName,
  		userImg: message.userImg,
  		cnvID: message.cnvID,
  		desig: message.desig,
  		groupName: message.groupName
  	  });
  	});

  	socket.on('shareThisMsg',function(data, callback){
  	  createPersonalConv( data.myID, data.targetID, 'personal', (result, err) =>{
        if (err) {
              if (err) throw err;
        } else if(result.status){
          callback(result);
        } else {
          console.log(result);
        }
      });
  	});

    socket.on('shareMessage', function(data, callback) {
      readOldMessage(data, (result, error) =>{
        // console.log(result);
        if(result.status){
          io.to(data.target_user).emit('newMessage', {
            msg_id: result.res,
            msg_from: data.sender_id,
            msg_text: result.message_data.msg_body,
            msg_sender_name: data.sender_name,
            msg_sender_img: data.sender_img,
            msg_attach_files: { attch_imgfile: result.message_data.attch_imgfile,
                                attch_audiofile: result.message_data.attch_audiofile,
                                attch_videofile: result.message_data.attch_videofile,
                                attch_otherfile: result.message_data.attch_otherfile
                              },
            msg_conv_id: data.conversation_id,
            msg_thread_root_id: false
          });
          callback(result);
        }else{
          callback({status:false});
        }
      });
    });

    socket.on('messageTagSave',function(data, callback){
      hasMessageThisTag(data.conversation_id,data.msg_id,data.userid,data.tagTitle,(respond,error)=>{
        if (error) {
          if (error) throw error;
        } else if(respond.status){
          callback({status:true, respond: respond});
        }else{
          callback({status:false, respond: respond});
        }
      });
    });

    socket.on('deleteTag',function(data, callback){
      deleteThisTag(data.tagid, (respond,error)=>{
        if (error) {
          if (error) throw error;
        } else if(respond.status){
          callback({status:true, respond: respond});
        }else{
          callback({status:false, respond: respond});
        }
      });
    });



    socket.on('groupCreateBrdcst', function(message, callback) {

          var str = message.memberList;
          var strUUID = message.memberListUUID;
          var adminList = message.adminList;
          var adminListUUID = message.adminListUUID;
          var memberlist = str.concat(adminList);
          var memberlistUUID = strUUID.concat(adminListUUID);
          var joinMenber = memberlist.join(',');
          if(message.teamname !== ""){
            createGroup(adminListUUID,memberlistUUID,message.teamname,message.createdby,message.selectecosystem,message.grpprivacy,message.conv_img,(result, err) =>{
              if(err){
                console.log(err);
              } else {
                if(result.status) {
                  socket.broadcast.emit('groupCreate', {
                    room_id: result,
                    memberList: strUUID,
                    adminList: adminListUUID,
                    selectecosystem: message.selectecosystem,
                    teamname: message.teamname,
                    grpprivacy: message.grpprivacy,
                    conv_img : message.conv_img
                  });
      		  callback(result);
                } else {
                  console.log(result);
                }
              }
            });
          }
        });

    socket.on('client_typing', function(message) {
      // console.log('line 112 room_id= ', message.room_id);
      // console.log('line 113 sender_id= ', message.sender_id);
      // console.log('line 114 conversation_id ', message.conversation_id);
      if(message.room_id == message.sender_id){
        io.to(message.conversation_id).emit('server_typing_emit', {display: message.display, room_id: message.room_id, sender_id: message.sender_id, img: message.sender_img, name: message.sender_name, conversation_id: message.conversation_id});
      } else {
        io.to(message.room_id).emit('server_typing_emit', {display: message.display, room_id: message.room_id, sender_id: message.sender_id, img: message.sender_img, name: message.sender_name, conversation_id: message.conversation_id});
      }
      // socket.broadcast.emit('typingBroadcast', {display: message.display, msgsenderroom: message.sendto, img: message.sender_img, name: message.sender_name});
    });

    socket.on('emoji_emit', function(data) {
        // console.log(data);
        io.to(data.room_id).emit('emoji_on_emit', {room_id: data.room_id, msg_id: data.msgid, emoji_name: data.emoji_name, count: data.count, sender_id: data.sender_id });
    });

    socket.on('group_join', function(group){
      socket.join(group.group_id);
    });

    socket.on('videocall_req', function (data,callback) {

      if(data.to_id){
        console.log('sendBusy from videocall_req');
        sendBusyMsg({ user_id : data.my_id , is_busy:1 }, (result1) =>{
          if(result1.status){
            console.log('sendBusy from videocall_req');
            sendBusyMsg({ user_id : data.to_id , is_busy:1 }, (result2) =>{
              if(result2.status){
                io.to(data.to_id).emit('videocall_send', data);
                callback(result2.status);
              }
            });
          }
        });
      }
    });

    socket.on('audiocall_req', function (data,callback) {

      if(data.to_id){

         sendBusyMsg({ user_id : data.my_id , is_busy:1 }, (result1) =>{

           if(result1.status){
             console.log('sendBusy1 from audiocall_req');

             sendBusyMsg({ user_id : data.to_id , is_busy:1 }, (result2) =>{
               if(result2.status){
                 console.log('sendBusy2 from audiocall_req');
                 io.to(data.to_id).emit('audiocall_send', data);
                 callback(result2.status);
               }
             });


           }
         });

      }

    });

    socket.on('call_hangup_before', function (data,callback) {
      if(data.hangup_id){
        sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
          if(result1.status){
            sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
              if(result2.status){

                if(data.reload_status==true){
                  sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call', data.call_duration, (result3, err) =>{
                    if(err){ console.log(err); }
                    else {

                      if(result3.status) {
                        _.merge(data, {msg_id: result3.res,
                              msg_from: data.caller_id,
                              msg_text: data.msgtext,
                              msg_sender_name: data.caller_name,
                              msg_sender_img: data.caller_img,
                              msg_conv_id: data.conversation_id,
                              msg_type: 'call'});

                        console.log('send.Call.Msg > call_ hangup_before : ' + data);
                        io.to(data.hangup_id).emit('send_hangup_before', data);
                        callback(data);
                      } else {
                        console.log(result3);
                      }
                    }
                  });
                }
                // else{
                //   io.to(data.hangup_id).emit('send_hangup_before', data);
                // }
              }
            });
          }
        });
      }
    });

    socket.on('call_hangup_after', function (data,callback) {

       if(data.hangup_id){

         sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result1) =>{

           if(result1.status){

             sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result2) =>{

               if(result2.status){
                  console.log('send.Call.Msg > call_ hangup_after'+data.reload_status);

                 if(data.reload_status==true){
                   sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext, data.msg_status,data.call_duration, (result3, err) =>{
                     if(err){
                       console.log(err);
                     } else {
                       if(result3.status) {
                        _.merge(data, {msg_id: result3.res,
                              msg_from: data.caller_id,
                              msg_text: data.msgtext,
                              msg_sender_name: data.caller_name,
                              msg_sender_img: data.caller_img,
                              msg_conv_id: data.conversation_id,
                              msg_type: 'call'});

                         io.to(data.hangup_id).emit('send_hangup_after', data);
                         callback(data);
                       } else {
                         console.log(result3);
                       }
                     }
                   });
                 }else{

                 }

               }
             });

           }
         });
       }

    });

    socket.on('call_reject_receiver', function (data,callback) {

       if(data.caller_id){

         sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
           if(result1.status){

             sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
               if(result2.status){

                 if(data.reload_status==true){
                 sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result3, err) =>{
                   if(err) console.log(err);
                   else {
                      if(result3.status) {

                        _.merge(data, {msg_id: result3.res,
                              msg_from: data.caller_id,
                              msg_text: data.msgtext,
                              msg_sender_name: data.sender_name,
                              msg_sender_img: data.sender_img,
                              msg_conv_id: data.conversation_id,
                              msg_type: 'call'});

                        io.to(data.caller_id).emit('send_reject_caller', data);

                        callback(data);
                      } else {
                        console.log(result3);
                      }
                   }
                 });
               }

               }
             });

           }
         });


       }

    });

    socket.on('call_reject_media_caller', function (data,callback) {

      if(data.caller_id){
        sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
          if(result1.status){
            sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
              if(result2.status){

                  if(data.reload_status==true){
                  sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result3, err) =>{
                    if(err){
                       console.log(err);
                    } else {
                      if(result3.status) {
                        _.merge(data, {msg_id: result3.res,
                            msg_from: data.caller_id,
                            msg_text: data.msgtext,
                            msg_sender_name: data.sender_name,
                            msg_sender_img: data.sender_img,
                            msg_conv_id: data.conversation_id,
                            msg_type: 'call'
                        });
                        console.log('send.Call.Msg > call_ reject_media_caller');
                        io.to(data.hangup_id).emit('send_reject_media_caller', data);
                        callback(data);
                      } else {
                        console.log(result3);
                      }
                    }
                  });
                }

              }
            });

          }
        });
      }

    });

    socket.on('call_reject_media_receiver', function (data,callback) {

     if(data.caller_id){

       sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
         if(result1.status){

           sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
             if(result2.status){
                if(data.reload_status==true){
                 sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result3, err) =>{
                   if(err){
                     console.log(err);
                   } else {
                     if(result3.status) {

                        _.merge(data, {msg_id: result3.res,
                              msg_from: data.caller_id,
                              msg_text: data.msgtext,
                              msg_sender_name: data.sender_name,
                              msg_sender_img: data.sender_img,
                              msg_conv_id: data.conversation_id,
                              msg_type: 'call'});
                       io.to(data.hangup_id).emit('send_reject_media_receiver', data);
                       callback(data);
                     } else {
                       console.log(result3);
                     }
                   }
                 });
               }

             }
           });

         }
       });


     }

    });

    socket.on('call_reject_network_caller', function (data,callback) {

       if(data.caller_id){

         sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
           if(result1.status){

             sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
               if(result2.status){

                 if(data.reload_status==true){
                   sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'network','', (result3, err) =>{
                     if(err){
                       console.log(err);
                     } else {
                       if(result3.status) {
                        _.merge(data, {msg_id: result3.res,
                              msg_from: data.caller_id,
                              msg_text: data.msgtext,
                              msg_sender_name: data.sender_name,
                              msg_sender_img: data.sender_img,
                              msg_conv_id: data.conversation_id,
                              msg_type: 'call'
                            });

                        console.log('send.Call.Msg > call_ reject_network_caller');
                         io.to(data.hangup_id).emit('send_reject_media_caller', data);
                         callback(data);
                       } else {
                         console.log(result3);
                       }
                     }
                   });
                 }

               }
             });

           }
         });


       }

    });

    socket.on('call_reject_network_receiver', function (data,callback) {

       if(data.caller_id){

         sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
           if(result1.status){

             sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
               if(result2.status){

                 if(data.reload_status==true){
                   sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'network','', (result3, err) =>{
                     if(err){
                       console.log(err);
                     } else {
                        if(result3.status) {
                          _.merge(data, {msg_id: result3.res,
                                msg_from: data.caller_id,
                                msg_text: data.msgtext,
                                msg_sender_name: data.sender_name,
                                msg_sender_img: data.sender_img,
                                msg_conv_id: data.conversation_id,
                                msg_type: 'call'
                              });

                          console.log('send.Call.Msg > call_ reject_network_receiver');
                          io.to(data.hangup_id).emit('send_reject_media_receiver', data);
                          callback(data);
                        } else {
                         console.log(result3);
                       }
                     }
                   });
                 }

               }
             });

           }
         });


       }

    });

    socket.on('call_noresponse', function (data,callback) {
      if(data.caller_id){
        sendBusyMsg({user_id: data.partner_id, is_busy: 0}, (result1) =>{
          if(result1.status){
            sendBusyMsg({user_id: data.caller_id, is_busy: 0}, (result2) =>{
              if(result2.status){
                console.log('send.Call.Msg > call_ noresponse reload_status',data.reload_status);
                if(data.reload_status==true){
                  sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result3, err) =>{
                    if(err){
                     console.log(err);
                    } else {
                      if(result3.status) {
                        _.merge(data, {msg_id: result3.res,
                           msg_from: data.caller_id,
                           msg_text: data.msgtext,
                           msg_sender_name: data.sender_name,
                           msg_sender_img: data.sender_img,
                           msg_conv_id: data.conversation_id,
                           msg_type: 'call'
                         });
                        console.log('send.Call.Msg > call_ noresponse',data);
                        io.to(data.caller_id).emit('send_noresponse', data);
                        callback(data);
                      } else {
                        console.log(result3);
                      }
                    }
                  });
                }
              }
            });
          }
        });
      }
    });

    socket.on('call_accept', function (data,callback) {

       if(data.caller_id){
         console.log('call_accept: '+data.caller_id);

         io.to(data.caller_id).emit('send_accept', data);
         callback(true);

       }

    });

    socket.on('get_isbusy_status', function (data,callback) {

      if(data.partner_id){
        var is_online=false;

        alluserlist.filter(function(el){
          if(el==data.partner_id) is_online=true;

        });

        if(is_online){

          getUserIsBusy(data.partner_id, (result1, err) =>{
            if(err){
              console.log(err);
            } else {

               if(result1.status==1){
                // user is busy
                sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result2, err) =>{

                  if(err){
                     console.log(err);
                  } else {
                    if(result2.status) {
                      console.log('send.Call.Msg > get_isbusy_status');

                      callback({
                        is_busy: result1.status,
                        is_online:is_online,
                        result:result2,
                        msg_id: result2.res,
                        msg_from: socket.handshake.session.userdata.from,
                        msg_text: data.msgtext,
                        msg_sender_name: data.sender_name,
                        msg_sender_img: data.sender_img,
                        msg_conv_id: data.conversation_id,
                        msg_type: 'call'
                      });
                    }
                  }
                });

               }else{
                  // user not busy
                  callback({
                    is_busy: result1.status,
                    is_online:is_online,

                  });
              }

            }
          });

        }else{
          // user not online

          sendCallMsg(data.user_id, data.sender_img, data.sender_name, data.conversation_id, data.msgtext,'call','', (result3, err) =>{

              if(err){
                console.log(err);
              } else {
                if(result3.status) {
                    console.log('send.Call.Msg > get_isbusy_status');
                    console.log(result3.res);
                    callback({
                      is_busy: 0,
                      is_online:is_online,
                      result:result3,
                      msg_id: result3.res,
                      msg_from: socket.handshake.session.userdata.from,
                      msg_text: data.msgtext,
                      msg_sender_name: data.sender_name,
                      msg_sender_img: data.sender_img,
                      msg_conv_id: data.conversation_id,
                      msg_type: 'call'
                    });
                }
              }
            });
        }
      }

    });


    /***********************************************************************/
    /***********************************************************************/
    socket.on('get_conversation_history', function(data, callback){
      hayvenjs.get_conversation(data, (demodata) =>{
        callback(demodata);
      });
    });

    // Get all public rooms from db where workspace define
    socket.on('public_conversation_history', function(data, callback){
      hayvenjs.public_conversation(data, (demodata) =>{
        callback(demodata);
      });
    });

    socket.on('updateKeySpace', function(message,callback){
      if(message.conversation_id != ""){
        updateKeySpace(message.conversation_id,message.keySpace,(result,err)=>{
          if(err){
            throw err;
          }else{
            callback(result);
          }
        });
      }
    });


    socket.on('updatePrivecy', function(message,callback){
      if(message.conversation_id != ""){
        updatePrivecy(message.conversation_id,message.grpprivacy,(result,err)=>{
          if(err){
            throw err;
          }else{
            callback(result);
          }
        });
      }
    });

    socket.on('updateRoomimg', function(message,callback){
      if(message.conversation_id != ""){
        updateRoomimg(message.conversation_id,message.conv_img,(result,err)=>{
          if(err){
            throw err;
          }else{
            callback(result);
          }
        });
      }
    });

    socket.on('saveTag', function(message,callback){
      if(message.currentConvID != ""){
        saveTag(message.created_by,message.conversation_id,message.tagTitle,(result,err)=>{
          if(err){
            throw err;
          }else{
            callback(result);
          }
        });

        // findtag(message.conversation_id,message.tagTitle,(fresult,ferr)=>{
        //     if(ferr){
        //       throw ferr;
        //     }else{
        //       var tagDetLen = fresult.tagDet;
        //       if(tagDetLen.length > 0){
        //           callback({status: false, err: 'at'}); //at = alreadt tagged
        //       }else{
        //         saveTag(message.created_by,message.conversation_id,message.tagTitle,(result,err)=>{
        //           if(err){
        //             throw err;
        //           }else{
        //             callback(result);
        //           }
        //         });
        //       }
        //
        //     }
        // });

      }
    });

    socket.on('send_message', function(message) {
      if(message.is_room === false){
        sendNewMsg( socket.handshake.session.userdata.from,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(72, err);
          } else {
            if(result.status) {
              if(socket.handshake.session.userdata.from.toString() !== message.to.toString()){
                io.to(message.to).emit('newMessage', result);
              }
              io.to(socket.handshake.session.userdata.from.toString()).emit('message_sent', result);
            } else {
              console.log(result);
            }
          }
        });
      }
      else if(message.is_room === true) {
        // console.log('something wrong!!!');
        // io.to('navigate_2018_902770').emit('newMessage', generateMessage(socket.handshake.session.userdata.from, message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img));
        // This is temporary group message.
        sendNewMsg( socket.handshake.session.userdata.from,
                    message.sender_img, message.sender_name,
                    message.conversation_id,
                    message.text,
                    message.attach_files, (result, err) =>{
          if(err){
            console.log(err);
          } else {
            if(result.status) {
              socket.broadcast.emit('newMessage', result); // this emit receive all users except me
              // io.to(message.conversation_id).emit('newMessage', {msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id}); // this emit receive all users except me
              // console.log({msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id});
              io.to(socket.handshake.session.userdata.from.toString()).emit('message_sent', result);
            } else {
              console.log(result);
            }
          }
        });
      }
    });

    socket.on('send_rep_message', function(message) {
        if(message.is_room === false){
            sendNewMsg( socket.handshake.session.userdata.from,
                message.sender_img, message.sender_name,
                message.conversation_id,
                message.text,
                message.attach_files, (result, err) =>{
                if(err){
                    console.log(72, err);
                } else {
                    if(result.status) {
                        if(socket.handshake.session.userdata.from.toString() !== message.to.toString()){
                            io.to(message.to).emit('newRepMessage', result);
                        }
                        io.to(socket.handshake.session.userdata.from.toString()).emit('newRepMessage', result);
                    } else {
                        console.log(result);
                    }
                }
            });
        }
        else if(message.is_room === true) {
            // console.log('something wrong!!!');
            // io.to('navigate_2018_902770').emit('newMessage', generateMessage(socket.handshake.session.userdata.from, message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img));
            // This is temporary group message.
            sendNewMsg( socket.handshake.session.userdata.from,
                message.sender_img, message.sender_name,
                message.conversation_id,
                message.text,
                message.attach_files, (result, err) =>{
                if(err){
                    console.log(err);
                } else {
                if(result.status) {
                    socket.broadcast.emit('newRepMessage', result); // this emit receive all users except me
                    // io.to(message.conversation_id).emit('newMessage', {msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id}); // this emit receive all users except me
                    // console.log({msg_id: result.res, msg_from: message.to, msg_text: message.text, msg_sender_name: message.sender_name, msg_sender_img: message.sender_img, msg_attach_files: message.attach_files, msg_conv_id: message.conversation_id});
                    io.to(socket.handshake.session.userdata.from.toString()).emit('newRepMessage', result);
                    } else {
                        console.log(result);
                    }
                }
            });
        }
    });
    /***********************************************************************/
    /***********************************************************************/

  });

  return router;
}
