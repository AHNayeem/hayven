var app = require('express');
var router = app.Router();
var _ = require('lodash');

var {models} = require('./../config/db/express-cassandra');

var saveConversation = (created_by, participants, title, callback) => {
  var conversation_id = models.uuid();
  var conversation = new models.instance.Conversation({
    conversation_id: conversation_id,
    created_by: models.uuidFromString(created_by),
    participants: participants,
    title: title
  });
  conversation.saveAsync().then(function() {
    callback({status:true, conversation_id});
  }).catch(function(err) {
    callback({status:false, err: err});
  });
};

var createGroup = (adminList,memberList,groupName,createdBy,ecosystem,grpprivacy,conv_img,callback) =>{
  var conversation_id = models.uuid();
  var conversation = new models.instance.Conversation({
      conversation_id: conversation_id,
      created_by: models.uuidFromString(createdBy),
      group: 'yes',
      group_keyspace:ecosystem,
      privacy:grpprivacy,
      single: 'no',
      participants_admin: adminList,
      participants: memberList,
      title: groupName,
      conv_img : conv_img
  });
  conversation.saveAsync().then(function() {
    callback({status:true, conversation_id});
  }).catch(function(err) {
    callback({status:false, err: err});
  });
};

var findConversationHistory = (conversation_id, callback) =>{
  models.instance.Messages.find({conversation_id: conversation_id}, {raw:true, allow_filtering: true}, function(err, conversation){
    if(err){
      callback({status: false, err: err});
    }else{
      callback({status: true, conversation: conversation});
    }
  });
};

var checkAdmin = (conversation_id,useruuid, callback) =>{
  models.instance.Conversation.find({conversation_id: models.timeuuidFromString(conversation_id),created_by: models.timeuuidFromString(useruuid)}, {raw:true, allow_filtering: true}, function(err, conversation){
    if(err){
      callback({status: false, err: err});
    }else{
      callback({status: true, conversation: conversation});
    }
  });
};

var check_only_Creator_or_admin = (conversation_id, useruuid, callback) =>{
  var query = {
    participants_admin: { $contains: useruuid },
    conversation_id: { $eq: models.uuidFromString(conversation_id) }
  };

  models.instance.Conversation.find(query,{ raw: true, allow_filtering: true }, function(err, conversation) {
    if(err){
      console.log("This is err",err);
      callback({status: false, err: err});
    }else{
      callback({status: true, conversation_id: conversation});
    }
  });
};


var createPersonalConv = (myID, frndID, ecosystem, callback) =>{

  var query = {
    participants: { $contains: myID},
    group: { $eq: 'no' },
    single: { $eq: 'yes' }
  };

  models.instance.Conversation.find(query,{ raw: true, allow_filtering: true }, function(err, conversation) {
    if(err){
      console.log("This is err",err);
      callback({status: false, err: err});
    }else{
      var resultCount = 0;
      var resultArray = 0;
      var ownCount = 0;

      if(myID === frndID){
        _.each(conversation, function(v, k) {
          if(v.participants.length == 1){
            if(v.participants[0]== frndID){
              resultArray = v.conversation_id;
            }
          }else{
            ownCount++;
          }
        });

        if(parseInt(ownCount) === parseInt(conversation.length)){
          var conversation_id = models.uuid();
          var memberList = [myID,frndID];
          var conversation = new models.instance.Conversation({
              conversation_id: conversation_id,
              created_by: models.uuidFromString(myID),
              group: 'no',
              group_keyspace:ecosystem,
              privacy:'private',
              single: 'yes',
              participants: memberList,
              title: 'Personal'
          });
          conversation.saveAsync().then(function() {
            callback({status:true, conversation_id});
          }).catch(function(err) {
            callback({status:false, err: err});
          });
        }else{
          callback({status: true, conversation_id: resultArray});
        }

      }else{
        _.each(conversation, function(v, k) {

          var result = _.find(v.participants, function (str, i) {
            if (str.match(frndID)){
              return true;
            }
          });

          if(result !== undefined){
            resultCount++;
            resultArray = v.conversation_id;
          }
        });

        if(resultCount>0){
          callback({status: true, conversation_id: resultArray});
        }else{
          var conversation_id = models.uuid();
          var memberList = [myID,frndID];
          var conversation = new models.instance.Conversation({
              conversation_id: conversation_id,
              created_by: models.uuidFromString(myID),
              group: 'no',
              group_keyspace:ecosystem,
              privacy:'private',
              single: 'yes',
              participants: memberList,
              title: 'Personal'
          });
          conversation.saveAsync().then(function() {
            callback({status:true, conversation_id});
          }).catch(function(err) {
            callback({status:false, err: err});
          });
        }
      }
    }
  });
};

var findConvDetail = (conversationid, callback) =>{
  console.log(conversationid);
  models.instance.Conversation.find({conversation_id: models.uuidFromString(conversationid) }, function(err, conversationDetail) {
    if (err) throw err;
    else callback({status:true, conversationDetail: conversationDetail});
  });
};

var saveNewGroup = (conversationMemList,ecosystem,crtUserID, callback) =>{
  var conversation_id = models.uuid();
  var conversation = new models.instance.Conversation({
      conversation_id: conversation_id,
      created_by: models.uuidFromString(crtUserID),
      group: 'yes',
      group_keyspace:ecosystem,
      privacy:'private',
      single: 'no',
      participants: conversationMemList,
      title: 'Group'
  });
  conversation.saveAsync().then(function() {
    callback({status:true, conversation_id});
  }).catch(function(err) {
    callback({status:false, err: err});
  });
};

var updateGroupName = (conversationid,newGroupname, callback) =>{
  var query_object = {conversation_id: models.uuidFromString(conversationid)};
  var gname = (newGroupname == '') ? 'Group':newGroupname;
  var update_values_object = {title: gname};
  var options = {ttl: 86400, if_exists: true};
  models.instance.Conversation.update(query_object, update_values_object, options, function(err){
      if(err) callback({status:false, err: err});
      else callback({status:true});
  });
};

var updateKeySpace = (conversation_id,keySpace, callback) =>{
  var query_object = {conversation_id: models.uuidFromString(conversation_id)};
  var update_values_object = {group_keyspace: keySpace};
  var options = {ttl: 86400, if_exists: true};
  models.instance.Conversation.update(query_object, update_values_object, options, function(err){
      if(err) callback({status:false, err: err});
      else callback({status:true});
  });
};

var updatePrivecy = (conversation_id,grpprivacy, callback) =>{
  var query_object = {conversation_id: models.uuidFromString(conversation_id)};
  var update_values_object = {privacy: grpprivacy};
  var options = {ttl: 86400, if_exists: true};
  models.instance.Conversation.update(query_object, update_values_object, options, function(err){
      if(err) callback({status:false, err: err});
      else callback({status:true});
  });
};

var updateRoomimg = (conversation_id,roomName, callback) =>{
  var query_object = {conversation_id: models.uuidFromString(conversation_id)};
  var update_values_object = {conv_img: roomName};
  var options = {ttl: 86400, if_exists: true};
  models.instance.Conversation.update(query_object, update_values_object, options, function(err){
      if(err) callback({status:false, err: err});
      else callback({status:true});
  });
};

var saveTag = (created_by,roomID, tagTitle, callback) => {

  var queries = [];

  _.each(tagTitle, function(v, k) {
    var tag_id = models.uuid();
    var tag = new models.instance.Tag({
      tag_id: tag_id,
      tagged_by: models.uuidFromString(created_by),
      conversation_id: models.timeuuidFromString(roomID),
      title: v
    });
    var save_query = tag.save({return_query: true});
    queries.push(save_query);

  });

  models.doBatch(queries, function(err){
      if(err){ throw err;}
      else {callback({status:true});}
  });


  // var tag_id = models.uuid();
  // var tag = new models.instance.Tag({
  //   tag_id: tag_id,
  //   tagged_by: models.uuidFromString(created_by),
  //   conversation_id: models.timeuuidFromString(roomID),
  //   title: tagTitle
  // });
  // tag.saveAsync().then(function() {
  //   callback({status:true, tag_id});
  // }).catch(function(err) {
  //   callback({status:false, err: err});
  // });
};

var findtag = (conversation_id,title, callback) =>{
  models.instance.Tag.find({conversation_id: models.timeuuidFromString(conversation_id),title: title}, {raw:true, allow_filtering: true}, function(err, tagDet){
    if(err){
      callback({status: false, err: err});
    }else{
      callback({status: true, tagDet: tagDet});
    }
  });
};

module.exports = {
  saveConversation,
  findConversationHistory,
  createGroup,
  checkAdmin,
  createPersonalConv,
  check_only_Creator_or_admin,
  findConvDetail,
  saveNewGroup,
  updateGroupName,
  updateKeySpace,
  updatePrivecy,
  updateRoomimg,
  saveTag,
  findtag
};
