var app = require('express');
var _ = require('lodash');
var fs = require('fs');
var request = require('request');
const metascraper = require('metascraper');
const got = require('got');
var router = app.Router();

var {models} = require('./../config/db/express-cassandra');

var hayvenjs = [];

var get_conversation = (conversation_id, callback) =>{
  models.instance.Messages.find({conversation_id: conversation_id}, {raw:true, allow_filtering: true}, function(err, conversation){
    if(err){
      callback({status: false, err: err});
    }else{
      callback({status: true, conversation: conversation});
    }
  });
};

hayvenjs["get_conversation"] = (data, callback) =>{
	// console.log('This is hayvenjs', data);
	models.instance.Conversation.find({conversation_id: models.timeuuidFromString(data.conversationid) }, function(err, conversationDetail) {
        if (err) throw err;

      	get_conversation(models.timeuuidFromString(data.conversationid), (result, error) => {
			       var conversation_list = _.sortBy(result.conversation, ["created_at",]);


			       models.instance.Pinned.findOne({user_id: models.uuidFromString(data.user_id), block_id:models.timeuuidFromString(data.conversationid)}, { allow_filtering: true }, function(err, pinnedBlocks){
				           if(err) throw err;

    				       models.instance.Tag.find({conversation_id: models.timeuuidFromString(data.conversationid)}, { allow_filtering: true }, function(tagserr, tags){
          					if(tagserr) throw tagserr;

          					var res_data = {
          						conversation_id: data.conversationid,
          						conversation_type: data.type,
          						conversation: conversationDetail,
          						room_id: data.id,
          						room_name: data.name,
          						room_img: data.img,
          						conversation_list: conversation_list,
          						tags: tags,
          						pinnedStatus: pinnedBlocks,
          					};

    					      callback(res_data);

    				});
    			});
      	});
  });
};


// Get all public rooms from db where workspace define
hayvenjs["public_conversation"] = (data, callback) =>{

	var query = {
      group_keyspace: { $eq: data.keySpace },
      single: { $eq: 'no' }
    };

	models.instance.Conversation.find( query, { raw: true, allow_filtering: true },function(err, rooms) {
        if (err) {
        	var res_data = {
				staus: false
			};
        }else{

        	models.instance.Tag.find( {}, { raw: true, allow_filtering: true },function(terr, tags) {
		        if (terr) {
		        	console.log(terr);
		        }else{


					var convID = [];
					var convTag = [];

					_.each(rooms, function(value,key){
						convID.push(value.conversation_id.toString());
					});

					_.each(tags, function(va,ke){
						if(convID.indexOf(va.conversation_id.toString()) !== -1){
							convTag.push({'cnvID':va.conversation_id.toString(),'title':va.title});
						}
					});

					var res_data = {
						rooms: rooms,
						convTag: convTag,
						staus: true
					};
					callback(res_data);
		        }
		    });


        }

  	});

};

module.exports = {hayvenjs};
