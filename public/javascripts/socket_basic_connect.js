/**
* When message form submit
**/
$('#msg').on('keydown', function(event) {
  var code = event.keyCode || event.which;
  if (code == 13 && !event.shiftKey) { //Enter keycode = 13
    event.preventDefault();
    msg_form_submit();
  }

  // When typing start into message box
  if (typing === false) {
    typing = true;
    socket.emit('client_typing', { display: true, room_id: to, sender_id: user_id, sender_name: user_fullname, sender_img: user_img, conversation_id: conversation_id });
    timeout = setTimeout(timeoutFunction, 2000);
  }
});

var msg_sending_process = (str) => {
  var is_room = (conversation_type == 'group') ? true : false;
  $("#ChatFileUpload").closest('form').trigger("reset");
  $("#FileComment").val("");
  $("#attach_chat_file_list").html("");
  socket.emit('send_message', {
    conversation_id: conversation_id, sender_img: user_img,
    sender_name: user_fullname, to: to,
    is_room: is_room, text: str,
    attach_files: filedata[0], thread_root_id: swap_conversation_id
  });
  filedata = [];
  formDataTemp = [];
};

socket.on('message_sent', function(data) {
    $('.typing-indicator').html("");
    var html = draw_msg(data.msg);
    $('#msg-container').append(html);
    scrollToBottom('.chat-page');
    $('#msg').html('').focus();
});

/**
* timeoutFunction call after 2 second typing start
**/
var timeoutFunction = () => {
  typing = false;
  socket.emit("client_typing", { display: false, room_id: to, sender_id: user_id, sender_name: user_fullname, sender_img: user_img, conversation_id: conversation_id });
  // console.log('timeout emit' + moment().format('m-s'));
};

/**
* Receive typing event and
* display indicator images hide and show
**/
socket.on('server_typing_emit', function(data) {
  if (data.sender_id != user_id) {
    if (conversation_id == data.conversation_id) {
      draw_typing_indicator(data.display, data.img, data.name);
    }
  }
});

/**
* When a new message come,
* Check user message container is opne or not.
* if open, it show's the message in the container
* else marked as a notification that new message arived
**/
socket.on('newMessage', function(message) {
  // console.log(message);
  if (to == message.msg.sender_id || conversation_id == message.msg.conversation_id) {
    $('.typing-indicator').html("");
    var html = draw_msg(message.msg);
    $('#msg-container').append(html);
    scrollToBottom('.chat-page');

    socket.emit('seen_emit', { msgid: message.msg.msg_id, senderid: to, receiverid: user_id, conversation_id: conversation_id });
  }
  else if($("#conv"+message.msg.conversation_id).length == 1){
    var count = $("#conv"+message.msg.conversation_id).find(".unreadMsgCount").html();
    count = Number(count)>0?Number(count)+1:1;
    $("#conv"+message.msg.conversation_id).find(".unreadMsgCount").html(count);
    display_show_hide_unread_bar(++total_unread_count);
  }

  
  // push notification
  Push.Permission.request();
  Push.create(message.msg.sender_name, {
      body: message.msg.msg_body,
      icon: "/images/users/"+message.msg.sender_img,
      timeout: 10000,
      onClick: function() {
          document.getElementById("conv"+message.msg.conversation_id).click();
      }
  });

  unread_msg_conv_intop();

});

/**
* When add new emoji reaction,
**/
socket.on('emoji_on_emit', function(data) {
    if(data.sender_id != user_id)
        append_reac_emoji(data.msg_id, '/images/emoji/' + data.emoji_name + '.png', 1);
});


/* Reply Messages */
$('#msg_rep').on('keydown', function(event) {
  var code = event.keyCode || event.which;
  if (code == 13 && !event.shiftKey) { //Enter keycode = 13
    event.preventDefault();
    var str = $('#msg_rep').html();
    str = str.replace(/(<\/?(?:img|br)[^>]*>)|<[^>]+>/ig, '$1'); // replace all html tag
    str = convert(str);
    str = str.replace(/&nbsp;/gi, '').trim();
    str = str.replace(/^(\s*<br( \/)?>)*|(<br( \/)?>\s*)*$/gm, '');
    if (str != "") {
        var is_room = (conversation_type == 'group') ? true : false;
        $("#ChatFileUpload").closest('form').trigger("reset");
        $("#FileComment").val("");
        $("#attach_chat_file_list").html("");

        socket.emit('send_rep_message', {
          conversation_id: thread_id, sender_img: user_img,
          sender_name: user_fullname, to: to,
          is_room: is_room, text: str,
          attach_files: filedata[0], thread_root_id: conversation_id
        });
        socket.emit('update_thread_count', { msg_id: thread_root_id });
        draw_rep_count(thread_root_id);
        filedata = [];
        formDataTemp = [];
        $("#msg_rep").html("");
        $("#msg_rep").focus();
    }
  }
});

socket.on('newRepMessage', function(message) {
    if(message.status){
        if (to == message.msg.sender_id || thread_id == message.msg.conversation_id) {
            draw_rep_msg(message.msg);
        }
    }
});
socket.on('update_thread_counter', function(data){
    draw_rep_count(data.msg_id);
})
/* End Reply Messages */
