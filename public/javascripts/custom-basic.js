function profileNav(){
	
	if($('.profilenavMainSection').is(":visible") == true){
		$('.profilenavMainSection').hide();
		$('.profile_nav img.nav_ico').css('transform', 'rotate(0deg)');
	}else{
		$('.profilenavMainSection').show();
		$('.profile_nav img.nav_ico').css('transform', 'rotate(180deg)');
	}
}


function logout(){
	
}

$(document).mouseup(function(e){
    var profileNav = $(".profile_nav");
    var profileNavContent = $('.profilenavMainSection');

    // if the target of the click isn't the container nor a descendant of the container
    if (!profileNav.is(e.target) && profileNav.has(e.target).length === 0)
    {
        profileNavContent.hide();
        $('.profile_nav img.nav_ico').css('transform', 'rotate(0deg)');
    }

});

var escKeyUpForHead = ()=>{
 	$(window).keyup(function(e){
 		if (e.keyCode == 27) {
 			if($('.profilenavMainSection').is(":visible") == true){
 				$('.profilenavMainSection').hide();
 				$('.profile_nav img.nav_ico').css('transform', 'rotate(0deg)');
 			}
 		}
 	});
 } 
 escKeyUpForHead();


 var sideBarActiveInactive = (event)=>{
 	$('.side_bar_list_item li').css({
		'border-left' : 'none',
		'background'  : 'transparent',
		'padding-left': '42px',
		'color' 	  : '#5A5A5A'
	});
	$('.side_bar_list_item li').children(".hash, .online, .offline, .lock, .toDo").css('left', '12px');
	$(event.target).css({
		'border-left' : '6px solid #1676EA',
		'background'  : 'rgba(22, 118, 234, 0.08)',
		'padding-left': '36px',
		'color'		  : 'rgba(0,0,0,0.88)'
	});
	$(event.target).children(".hash, .online, .offline, .lock, .toDo").css('left', '6px');
 }