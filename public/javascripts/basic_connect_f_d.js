

var searchsldefclas = (value) => {
	$(".s-l-def-clas").each(function() {
		if ($(this).text().toLowerCase().search(value.toLowerCase()) > -1) {
			$(this).parent('li').show();
		}else {
			$(this).parent('li').hide();
		}
	});

	$('.s-l-def-clas').unhighlight();
	$('.s-l-def-clas').highlight(value);
}


var searchRooms = (value) => {

	if(value == ''){
		$(".chanel-name").each(function() {
			$(this).parent('div').show();
			$(this).parent('div:nth-child(3n)').css('margin-right','0px');
		});
	}else{
		$(".chanel-name").each(function() {
			if ($(this).text().toLowerCase().search(value.toLowerCase()) > -1) {
				$(this).parent('div').show();

				// $(this).parent('div').css('margin-right','16px');
				// $(this).parent('div:nth-child(3n)').css('margin-right','0px');
			}else {
				$(this).parent('div').hide();
			}
		});

		$(".chanel-name:visible").each(function() {
			$(this).parent('div:visible:nth-child(3n)').css('margin-right','0px');
		});
	}

	$('.chanel-name').unhighlight();
	$('.chanel-name').highlight(value);
}

$.each(allUserdata[0].users, function(ky,va){
	if(jQuery.inArray(va.dept, dept) == -1){
	dept.push(va.dept);
	}
});

$.each(dept, function(k,v){
	var ulDes = '<li>';
		ulDes += '      <div class="department">'+v+'</div>';
		ulDes += '      <div class="dpt-members">';
		ulDes += '        <ul class="suggested-list" id="s-l-'+v.replace(/\s/g,'')+'" >';
		ulDes += '        </ul>';
		ulDes += '      </div>';
		ulDes += '    </li>';

		$("#s-m-ul").append(ulDes);
});
// Render all user accoroding to designation
$.each(allUserdata[0].users, function(ky,va){
	if(va.id == user_id){
	var adminDes = '<li>';
		adminDes += '        <img src="/images/users/'+va.img+'" class="profile">';
		adminDes += '        <span class="name" data-uuid="'+va.id+'">'+va.fullname+'</span> <span class="is-admin">(Admin)</span>';
		adminDes += '      </li>';
	$("#memberlist").append(adminDes);
	}else{
	var liDes = '<li>';
		liDes += '      <img src="/images/users/'+va.img+'" class="profile">';
		liDes += '      <spna class="name" data-uuid="'+va.id+'">'+va.fullname+'</spna> <spna class="designation-name">'+va.designation+'</spna>';
		liDes += '    </li>';
	var dept = va.dept;
	$("#s-l-"+dept.replace(/\s/g,'')).append(liDes);
	}

	var definedList = '<li>';
		definedList += '      <img src="/images/users/'+va.img+'" class="profile">';
		definedList += '      <spna class="name s-l-def-clas" data-uuid="'+va.id+'">'+va.fullname+'</spna> <spna class="designation-name">'+va.designation+'</spna>';
		definedList += '    </li>';

	$("#s-l-def").append(definedList);
	$("#directMsgUserList").append(definedList);
});

/** Add suggested user list to
	 selected group member list */
var directMsgCont = 1;
var directMsgName = "";
var directMsgUUID = "";
var directMsgImg = "";
var directMsgSubtitle = "";

$('.suggested-list li').on('click', function(){

	var img_src = $(this).find('img').attr('src');
	var name = $(this).find('.name').text();
	var uuid = $(this).find('.name').attr('data-uuid');
	var subtitle = $(this).find('.designation-name').text();

	if($("#createDirMsgContainer").is(":visible") && $("#roomIdDiv").attr('data-rfu') == ''){
		if(directMsgCont == 1){
			memberList.push(name);
			memberListUUID.push(uuid);
			directMsgCont++;
			directMsgName = name;
			directMsgUUID = uuid;
			directMsgImg = img_src;
			directMsgSubtitle = subtitle;
			group_member_li_draw(name, img_src, uuid,'0','0',subtitle);
			all_action_for_selected_member();
		}else{
			toastr["warning"]("Multiple member is not allowed in direct message", "Warning");
		}
	}else if($("#roomIdDiv").attr('data-rfu') == 'ready'){
		var roomid = $("#roomIdDiv").attr('data-roomid');
		var roomTitle = $("#roomIdDiv").attr('data-title');

		if (jQuery.inArray(uuid, participants) === -1){
			$.ajax({
	            type: 'POST',
	            data: {
	                conversation_id: roomid,
	                targetID: uuid
	            },
	            dataType: 'json',
	            url: '/hayven/groupMemberAdd',
	            success: function(data) {
	                participants.push(uuid);
	                group_member_li_draw(name, img_src, uuid,'ready',roomid,subtitle);
				}
	        });
		}else{
			toastr["warning"](name+" is a member of \""+roomTitle+"\" room", "Warning");
		}

	}else{

		if($(".inviteMember").length == 0){
			$(".memberList").show();
		}

		if(jQuery.inArray(name, memberList) !== -1){

		}else{
			memberList.push(name);
			memberListUUID.push(uuid);
			$("#numbers").text(parseInt(memberList.length)+1);

			group_member_li_draw(name, img_src, uuid,'0','0',subtitle);
		}

		all_action_for_selected_member();
	}

	$(this).remove();

});

$('.add-team-member').on('keyup', function(e){
	var str = $(e.target).val();
	if(str != ""){
	$('.suggested-type-list').show();
	if(str.indexOf('@') != -1){
		$('.suggested-type-list li').hide();
		send_email_invite(str);
	}
	} else {
	$('.suggested-type-list').hide();
	}
});

$('.add-direct-member').on('keyup', function(e){
	var str = $(e.target).val();
	if(str != ""){

		$('.suggested-type-list').show();
		$('.suggested-type-list>ul').css('width','323px !important');
		if(str.indexOf('@') != -1){
			$('.suggested-type-list li').hide();
			send_email_invite(str);
		}
	} else {
		$('.suggested-type-list').hide();
	}
});

$(".ml-listHl .member-div").mouseenter(function(e) {
    $(this).find('.add-admin').show();
    $(this).find('.remove-it').show();
}).mouseleave(function() {
    $(this).find('.add-admin').hide();
    $(this).find('.remove-it').hide();
});

$(".ml-listHA .member-div").mouseenter(function(e) {
    $(this).find('.remove-admin').show();
    $(this).find('.remove-it').show();
}).mouseleave(function() {
    $(this).find('.remove-admin').hide();
    $(this).find('.remove-it').hide();
});

/*
 Create Chat Group on click create btn
*/

var getInfo = (event)=>{
	var tmppath = URL.createObjectURL(event.target.files[0]);
    $("#demoImg").attr('src',URL.createObjectURL(event.target.files[0]));
};

var roomImageUpdate = (roomid,event)=>{
	console.log(roomid);
	var formData = new FormData($('#roomImg')[0]);
	$.ajax({
		url: '/hayven/convImg',
		type: "POST",
		data: formData,
		dataType: 'json',
		contentType: false,
		processData: false,
		success: function(res){
			console.log(res);
			socket.emit('updateRoomimg', {
			    conversation_id: roomid,
			    conv_img: res.filename
			}, (callBack) => {
				var tmppath = URL.createObjectURL(event.target.files[0]);
				$("#demoImg").attr('src',URL.createObjectURL(event.target.files[0]));
			});
		}
	});
}

var CreateGroup = () =>{

	var teamname = $("#team-name").val();
	var selectecosystem = $("#select-ecosystem").val();
	var grpprivacy = 'public';
	if(teamname.length > 17){
		var over_length  = "over_length";
	}

	if ($("#grpPrivacy").is(":checked")) {
		grpprivacy = 'private';
	}else{
		grpprivacy = 'public';
	}

	if(teamname != ""){
		if(selectecosystem != ""){
			if(grpprivacy != ""){

			adminList.push(user_fullname);
			adminListUUID.push(user_id);

			$(".backWrap").hide();
			$(".new-group-chat-popup").hide();

			}else{
			$("#grp-privacy").css('border','1px solid RED');
			}
		}else{
			$("#select-ecosystem").css('border','1px solid RED');
		}
	}else{
		$("#team-name").css('border','1px solid RED');
	}

	// console.log($("#upload-channel-photo").val());

	if($("#upload-channel-photo").val() != ""){
		var formData = new FormData($('#roomImg')[0]);
		$.ajax({
		  url: '/hayven/convImg',
		  type: "POST",
		  data: formData,
		  dataType: 'json',
		  contentType: false,
		  processData: false,
		  success: function(res){
		  	console.log(res.filename);
		    socket.emit('groupCreateBrdcst', {
				createdby: user_id,
				memberList: memberList,
				memberListUUID: memberListUUID,
				adminList: adminList,
				adminListUUID: adminListUUID,
				is_room: '6',
				teamname: teamname,
				selectecosystem: selectecosystem,
				grpprivacy: grpprivacy,
				conv_img : res.filename
			},
			function(confirmation){
				$("#defaultRoom").remove();
				$("#channelList").prepend('<li onclick="start_conversation(event)" data-subtitle="'+selectecosystem+'" data-id="'+user_id+'" data-conversationtype="group" data-tm= "'+memberListUUID.length+'" data-conversationid="'+confirmation.conversation_id+'" data-name="'+teamname+'" data-img="feelix.jpg"  id="conv'+confirmation.conversation_id+'" class="'+over_length+'"><span class="hash"></span> '+teamname+'<span class="sub-text"> - '+selectecosystem+'</span></li>');
				$("#team-name").val("");
				$("#ml-admintype").hide();
				$("#ml-membertype").hide();

				$("#ml-listHA").html('');
				$("#ml-listHl").html('');
				$("#grpPrivacy").prop("checked",false);
				closeRightSection();
				$('#conv'+confirmation.conversation_id).click();
				tooltipForOverLength();

			});
		  }
		});
	}else{
		socket.emit('groupCreateBrdcst', {
			createdby: user_id,
			memberList: memberList,
			memberListUUID: memberListUUID,
			adminList: adminList,
			adminListUUID: adminListUUID,
			is_room: '6',
			teamname: teamname,
			selectecosystem: selectecosystem,
			grpprivacy: grpprivacy,
			conv_img : 'feelix.jpg'
		},
		function(confirmation){
			$("#defaultRoom").remove();
			$("#channelList").prepend('<li onclick="start_conversation(event)" data-subtitle="'+selectecosystem+'" data-id="'+user_id+'" data-conversationtype="group" data-tm= "'+memberListUUID.length+'" data-conversationid="'+confirmation.conversation_id+'" data-name="'+teamname+'" data-img="feelix.jpg"  id="conv'+confirmation.conversation_id+'" class="'+over_length+'"><span class="hash"></span> '+teamname+'<span class="sub-text"> - '+selectecosystem+'</span></li>');
			$("#team-name").val("");
			$("#ml-admintype").hide();
			$("#ml-membertype").hide();

			$("#ml-listHA").html('');
			$("#ml-listHl").html('');
			$("#grpPrivacy").prop("checked",false);
			closeRightSection();
			$('#conv'+confirmation.conversation_id).click();
			tooltipForOverLength();

		});
	}

}

$(document).keyup(function(e) {
	if (e.keyCode == 27) { // escape key maps to keycode `27`

		if ($('.suggested-type-list').is(':visible')) {
			$('.suggested-type-list').hide();
			$('.add-team-member').val("");
			$('.add-direct-member').val("");
		}
	}
});


/** On mouse up event 'mouseup' */
$(document).mouseup(function(e) {
	var container = $('.suggested-type-list');

	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container.hide();
		$('.add-team-member').val("");
	}
});

$(".remove-suggested-type-list").click(function(){
	$('.suggested-type-list').hide();
	$('.add-team-member').val("");
});



/** action for selected member list */
var all_action_for_selected_member = () =>{
  	/** On hover into the selected group member list,
      if member is already admin, show the remove admin btn,
      if member is not admin, show the make admin btn */

  	$(".ml-listHl .member-div").mouseenter(function(e) {
    	$(this).find('.add-admin').show();
    	$(this).find('.remove-it').show();
	}).mouseleave(function() {
	    $(this).find('.add-admin').hide();
	    $(this).find('.remove-it').hide();
	});

	$(".ml-listHA .member-div").mouseenter(function(e) {
	    $(this).find('.remove-admin').show();
	    $(this).find('.remove-it').show();
	}).mouseleave(function() {
	    $(this).find('.remove-admin').hide();
	    $(this).find('.remove-it').hide();
	});
  	/** When click on the make admin btn,
      admin text show and hide add admin btn */

  	/** When click on the remove admin btn,
      admin text null and hide remove admin btn */
	$(".remove-admin").on('click', function(){
		$(this).closest('li').find('.is-admin').text('').hide();
		$(this).closest('li').find('.remove-admin').hide();
		adminList.splice($(this).closest('li').find('.name').text(),1);
		adminList.splice($(this).closest('li').find('.name').attr('data-uuid'),1);
		memberList.push($(this).closest('li').find('.name').text());
		memberListUUID.push($(this).closest('li').find('.name').attr('data-uuid'));
	});

	$('.remove-it').on('click', function(e){

		e.preventDefault();
		e.stopImmediatePropagation();

		var name = $(this).closest('div').find('.name').text();
		var img = $(this).closest('div').find('img').attr('src');
	    var uuid = $(this).closest('div').find('.name').attr('data-uuid');

	    if(!$(this).hasClass('GroupFlRight')){
	    	directMsgCont = 1;

		    removeA(memberList, name);
		    removeA(memberListUUID, uuid);
		    $("#numbers").text(parseInt(memberList.length)+1);
			$(this).closest('div').remove();
			if($(".ml-listHl>.member-div").length == 0){
				$("#ml-membertype").hide();
			}

			var definedList = '<li>';
				definedList += '      <img src="'+img+'" class="profile">';
				definedList += '      <spna class="name s-l-def-clas" data-uuid="'+uuid+'">'+name+'</spna>';
				definedList += '    </li>';

			$("#s-l-def").append(definedList);
			$("#directMsgUserList").append(definedList);

			all_action_for_selected_member();
	    }


	});

	$('.suggested-list li').on('click', function(){

		var img_src = $(this).find('img').attr('src');
		var name = $(this).find('.name').text();
		var uuid = $(this).find('.name').attr('data-uuid');
		var subtitle = $(this).find('.designation-name').text();

		if($("#createDirMsgContainer").is(":visible") && $("#roomIdDiv").attr('data-rfu') == ''){
			if(directMsgCont == 1){
				memberList.push(name);
				memberListUUID.push(uuid);
				directMsgCont++;
				directMsgName = name;
				directMsgUUID = uuid;
				directMsgImg = img_src;
				directMsgSubtitle = subtitle;
				group_member_li_draw(name, img_src, uuid,'0','0',subtitle);
				all_action_for_selected_member();
			}else{
				toastr["warning"]("Multiple member is not allowed in direct message", "Warning");
			}
		}else if($("#roomIdDiv").attr('data-rfu') == 'ready'){
			var roomid = $("#roomIdDiv").attr('data-roomid');
			var roomTitle = $("#roomIdDiv").attr('data-title');

			if (jQuery.inArray(uuid, participants) === -1){
				$.ajax({
		            type: 'POST',
		            data: {
		                conversation_id: roomid,
		                targetID: uuid
		            },
		            dataType: 'json',
		            url: '/hayven/groupMemberAdd',
		            success: function(data) {
		                participants.push(uuid);
		                group_member_li_draw(name, img_src, uuid,'ready',roomid,subtitle);
					}
		        });
			}else{
				toastr["warning"](name+" is a member of \""+roomTitle+"\" room", "Warning");
			}

		}else{

			if($(".inviteMember").length == 0){
				$(".memberList").show();
			}

			if(jQuery.inArray(name, memberList) !== -1){

			}else{
				memberList.push(name);
				memberListUUID.push(uuid);
				$("#numbers").text(parseInt(memberList.length)+1);

				group_member_li_draw(name, img_src, uuid,'0','0',subtitle);
			}

			all_action_for_selected_member();
		}

		$(this).remove();

	});



};

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

/** selected group member list row html */
var group_member_li_draw = (name, img, uuid,urf,roomid,designation) =>{
	if(urf === 'ready'){
		var immg = img.split("/");
		var mldesign = '<div class="member-div" id="member' + uuid + '">';
	        mldesign += '          <img src="/images/users/' + immg[3] + '" class="member-img">';
	        mldesign += '          <div class="member-name">' + name + '</div>';
	        mldesign += '          <img src="/images/remove_8px_200 @1x.png" class="remove-it GroupFlRight" onclick = "removeMember(\'member\',\'' + uuid + '\',\'' + roomid + '\');">';
	        mldesign += '          <img src="/images/admin-remove_12px @1x.png" class="remove-admin remove2 GroupFlRight arfImg" onclick="makeMember(\'' + immg[3] + '\',\'' + name + '\',\'' + designation + '\',\'' + uuid + '\',\'' + roomid + '\')">';
	        mldesign += '          <img src="/images/admin-add_12px @1x.png" class="add-admin add2 GroupFlRight arfImg"  onclick="makeAdmin(\'' + immg[3] + '\',\'' + name + '\',\'' + designation + '\',\'' + uuid + '\',\'' + roomid + '\')">';
	        mldesign += '        </div>';
	    $("#ml-listHl").append(mldesign);
	}else{
		var html =  '<div class="member-div" id="member' + uuid + '">';
			html+=    '<img src="'+ img +'" class="member-img">';
			html +=   '<div data-uuid="'+uuid+'" class="member-name name">' + name + '</div>';
			html+=    '<img src="/images/remove_8px_200 @1x.png" class="remove-it">';
			html+=  '</div>';

		$('.ml-listHl').append(html);
	}

	$('.no-of-group-members').text($('.selected-group-members li:visible').length);
	$('.suggested-type-list').hide();
	$('.add-team-member').val("");

	$(".ml-listHl .member-div").mouseenter(function(e) {
        $(this).find('.add-admin').show();
        $(this).find('.remove-it').show();

    }).mouseleave(function() {
        $(this).find('.add-admin').hide();
        $(this).find('.remove-it').hide();
    });

    if ($('#ml-listHA .member-div').length > 0) {
        $("#ml-admintype").show();
    } else {
        $("#ml-admintype").hide();
    }

    if ($('#ml-listHl .member-div').length > 0) {
        $("#ml-membertype").show();
    } else {
        $("#ml-membertype").hide();
    }
};

$('div').on('click', '.pin-to-bar', function(event) {
	event.stopImmediatePropagation();
	event.stopPropagation();

	var conversationid = $(event.target).attr('data-conversationid');
	if(conversationid != ""){
		if ($(event.target).hasClass('pined')) {
			var pinnedid = $(event.target).attr('data-pinned');
			var blockID = $(event.target).attr('data-conversationid');
			var subtitle = $(event.target).attr('data-subtitle');
			var img = $(event.target).attr('data-img');
			var name = $(event.target).attr('data-name');
			var type = $(event.target).attr('data-type');

			$.ajax({
				type: 'POST',
				data: {
					pinnedNumber: '',
					targetID: pinnedid,
					blockID: '',
					type: 'unpin'
				},
				dataType: 'json',
				url: '/hayven/pinning',
				success: function(data) {
					$("#pin-to-bar").removeClass('pined');
					$("#pin-to-bar").attr('src','/images/basicAssets/custom_not_pin.svg');
					var sapnClasses = $("#conv"+blockID).find('span').attr('class');
					var splitClass = sapnClasses.split(' ');
					var status = '';

					if($.inArray('online',splitClass) !== -1){
						status = 'online';
					}else if($.inArray('offline',splitClass) !== -1){
						status = 'offline';
					}

					$("#conv"+blockID).remove();

					var design = '<li onclick="start_conversation(event)" data-id="'+blockID+'" data-subtitle="'+subtitle+'" data-conversationtype="'+type+'" data-conversationid="'+blockID+'" data-name="'+name+'" data-img="'+img+'" id="conv'+blockID+'">';
						design += '<span class="'+(type == "personal" ? status+" online_"+blockID:"hash")+'"></span>'+name+'';
						design += '</li>';
					if(type == 'personal'){

						if(name == user_fullname && blockID == user_id){
							$("#directListUl").prepend(design);
						}else{
							$("#directListUl").append(design);
						}

						// console.log($("#conv"+blockID).find('span').attr('class'));
					}else if(type == 'group'){
						$("#channelList").append(design);
					}
				}
			});

		} else {

			var pinnedCount = $('.pin').length;
			var PinnedNumber = parseInt(pinnedCount) + 1;
			var targetID = $(event.target).attr('data-id');
			var blockID = $(event.target).attr('data-conversationid');
			var subtitle = $(event.target).attr('data-subtitle');
			var img = $(event.target).attr('data-img');
			var name = $(event.target).attr('data-name');
			var type = $(event.target).attr('data-type');

			$.ajax({
				type: 'POST',
				data: {
					pinnedNumber: PinnedNumber,
					targetID: targetID,
					blockID: blockID,
					type: 'pin'
				},
				dataType: 'json',
				url: '/hayven/pinning',
				success: function(data) {
					$("#pin-to-bar").addClass('pined');
					$("#pin-to-bar").attr('src','/images/basicAssets/custom_pinned.svg');
					$("#pin-to-bar").attr('data-pinned',data.pinID);

					var sapnClasses = $("#conv"+blockID).find('span').attr('class');
					var splitClass = sapnClasses.split(' ');
					var status = '';

					if($.inArray('online',splitClass) !== -1){
						status = 'online';
					}else if($.inArray('offline',splitClass) !== -1){
						status = 'offline';
					}

					$("#conv"+blockID).remove();
					var design = '<li onclick="start_conversation(event)" data-id="'+blockID+'" data-subtitle="'+subtitle+'" data-conversationtype="'+type+'" data-conversationid="'+blockID+'" data-name="'+name+'" data-img="'+img+'" id="conv'+blockID+'">';
					design += '<span class="'+(type == "personal" ? status+" online_"+blockID:"hash")+'"></span>'+name+'';
					design += '</li>';
					$("#pintul").append(design);
				}
			});
		}
	}

});

var createDirectmsg = () =>{

	if(directMsgName != "" && directMsgUUID != ""){
		$.ajax({
			type: 'POST',
			data: {
				targetUser: directMsgName,
				targetID: directMsgUUID,
				ecosystem: 'Navigate'
			},
			dataType: 'json',
			url: '/hayven/personalConCreate',
			success: function(data) {
				var immg = directMsgImg.split("/");
				var design = '<li onclick="start_conversation(event)" data-id="'+directMsgUUID+'" data-subtitle="'+directMsgSubtitle+'" data-conversationtype="personal" data-conversationid="'+data.conversation_id+'" data-name="'+directMsgName+'" data-img="'+immg[3]+'" id="conv'+data.conversation_id+'">';
				design += '<span class="online"></span>'+directMsgName+'';
				design += '</li>';
				$("#directListUl").append(design);
				closeRightSection();
				$('#conv'+data.conversation_id).click();

			},
			error: function(err) {
				console.log(err);
			}
		});
	}else{
		toastr["warning"]("Unable to start direct message", "Warning");
	}

}
var getPublicData = (keySpace) =>{
	socket.emit('public_conversation_history', {keySpace}, (respons) =>{
		console.log(respons);
		if(respons.staus){
			$("#publicRoomsList").html("");
			$(".connect_right_section").hide();
			$('#joinChannelPanel').show();
			$.each(respons.rooms, function(k,v){
				// if($.inArray(user_id, v.participants_admin) == -1){

				// }
				var ststus = (v.privacy == "public" ? "hash":"lock");
				if($.inArray(user_id, v.participants) !== -1){
					var totalMember = v.participants;
					var roomDesign =  '<div class="added-channels">';
				        roomDesign += '		<div class="channel-joined"><img src="/images/basicAssets/joined.png" alt="">Joined</div>';
				        roomDesign += '		<h3 class="chanel-name" id="joinChanelTile'+v.conversation_id+'" data-roomid="'+v.conversation_id+'" data-rfu="join" data-title="'+v.title+'" data-privecy="'+v.privacy+'" data-keyspace="'+v.group_keyspace+'" data-participants="'+v.participants+'" data-admin="'+v.participants_admin+'" onclick="roomFromJOin($(this).attr(\'data-participants\'),$(this).attr(\'data-admin\'),$(this).attr(\'data-roomid\'),$(this).attr(\'data-title\'),$(this).attr(\'data-privecy\'),$(this).attr(\'data-keyspace\'))"><span class="'+ststus+'"></span>'+v.title+'</h3>';
				        roomDesign += '		<p class="channel-members"><img src="/images/basicAssets/Users.svg" alt="">'+totalMember.length+' Members</p>';
				        roomDesign += '		<div class="channel-tags">';
				        $.each(respons.convTag, function(ck,cv){
				        	if(cv.cnvID == v.conversation_id){
				        		roomDesign += '  		<p>'+cv.title+'</p>';
				        	}
				        });
				        roomDesign += '		</div>';
				        roomDesign += '		<h3 class="click-to-join" style="display: none">Join Channel</h3>';
				        roomDesign += '		<h3 class="click-to-leave">Leave Room</h3>';
				      	roomDesign += '</div>';

				    $("#publicRoomsList").append(roomDesign);
				}else{
					var totalMember = v.participants;
					var roomDesign =  '<div class="added-channels">';
				        roomDesign += '		<h3 class="chanel-name" id="joinChanelTile'+v.conversation_id+'" data-roomid="'+v.conversation_id+'" data-rfu="join" data-title="'+v.title+'" data-privecy="'+v.privacy+'" data-keyspace="'+v.group_keyspace+'" data-participants="'+v.participants+'" data-admin="'+v.participants_admin+'" onclick="roomFromJOin($(this).attr(\'data-participants\'),$(this).attr(\'data-admin\'),$(this).attr(\'data-roomid\'),$(this).attr(\'data-title\'),$(this).attr(\'data-privecy\'),$(this).attr(\'data-keyspace\'))"><span class="'+ststus+'"></span>'+v.title+'</h3>';
				        roomDesign += '		<p class="channel-members"><img src="/images/basicAssets/Users.svg" alt="">'+totalMember.length+' Members</p>';
				        roomDesign += '		<div class="channel-tags">';
				        $.each(respons.convTag, function(ck,cv){
				        	if(cv.cnvID == v.conversation_id){
				        		roomDesign += '  		<p>'+cv.title+'</p>';
				        	}
				        });
				        roomDesign += '		</div>';
				        roomDesign += '		<h3 class="click-to-join">Join Room</h3>';
				    	roomDesign += '</div>';
				    $("#publicRoomsList").append(roomDesign);
				}
			});
		}
	});
};


var joinRoom = (roomId,user_id, title) =>{
	var selectecosystem = $("#workspaceList").val();
	$.ajax({
        type: 'POST',
        data: {
            conversation_id: roomId,
            targetID: user_id
        },
        dataType: 'json',
        url: '/hayven/groupMemberAdd',
        success: function(data) {
            if(data == 'success'){
            	$("#roomJoin"+roomId).show();
            	$("#roomBtn"+roomId).removeClass('click-to-join').addClass('click-to-leave');
            	$("#roomBtn"+roomId).text("Leave Room");
            	$("#channelList").prepend('<li onclick="start_conversation(event)" data-subtitle="'+selectecosystem+'" data-id="'+user_id+'" data-conversationtype="group" data-conversationid="'+roomId+'" data-name="'+title+'" data-img="feelix.jpg"  id="conv'+roomId+'"><span class="hash"></span> '+title+'</li>');
							$("#roomBtn"+roomId).attr("onclick","leaveRoom('"+roomId+"','"+user_id+"')");
            }

        }
    });
};

var leaveRoom = (roomId,user_id) =>{
	var roomTitle = $("#roomTitle"+roomId).text();
	$.ajax({
        type: 'POST',
        data: {
            conversation_id: roomId,
            targetID: user_id
        },
        dataType: 'json',
        url: '/hayven/leaveRoom',
        success: function(data) {
            if(data == 'success'){
            	$("#roomJoin"+roomId).hide();
            	$("#roomBtn"+roomId).removeClass('click-to-leave').addClass('click-to-join');
            	$("#roomBtn"+roomId).text("Join Room");
            	$("#conv"+roomId).remove();
            	$("#roomBtn"+roomId).attr("onclick","joinRoom('"+roomId+"','"+user_id+"','"+roomTitle+"')");
            }
        }
    });
};

var roomEdit = (roomid,title,privecy,keyspace,rromimg) =>{

	$(".connect_right_section").hide();
	$('#createChannelContainer').show();
	$('.memberList').show();
	$("#ml-listHl").html("");
	$("#ml-listHA").html("");
	$("#team-name").val(title);
	$("#demoImg").attr('src','/upload/'+rromimg);

	$("#upload-channel-photo").attr("onchange","roomImageUpdate(\'"+roomid+"\',event)");

	$(".submitBtn").hide();
	$(".create-channel-heading").text("Update Room");

	// This line use for checking room update or room create
	$("#roomIdDiv").attr('data-rfu','ready');

	$("#select-ecosystem option").each(function(){
        if($(this).val()== keyspace){ // match here
            $(this).attr("selected","selected");
        }
    });

	if(privecy == 'private'){
		$('#grpPrivacy').attr('checked', true);
	}

	if ($.inArray(user_id, adminArra) === -1) {
		$(".add-team-member").prop("disabled",true);
		$('#grpPrivacy').prop("disabled",true);
	}

	$("#s-l-def").html("");
	$("#directMsgUserList").html("");

	$.each(allUserdata[0].users, function(ky, va) {
	    $.each(participants, function(k, v) {
	        if (va.id === v) {
	            if (jQuery.inArray(v, adminArra) === -1) {
	                var mldesign = '<div class="member-div" id="member' + va.id + '">';
	                mldesign += '          <img src="/images/users/' + va.img + '" class="member-img">';
	                mldesign += '          <div class="member-name">' + va.fullname + '</div>';
	                // mldesign += '          <div class="member-designation">' + va.designation + '</div>';
	                if ($.inArray(user_id, adminArra) !== -1) {
	                	mldesign += '          <img src="/images/remove_8px_200 @1x.png" class="remove-it GroupFlRight" onclick = "removeMember(\'member\',\'' + va.id + '\',\'' + roomid + '\');">';
	                	mldesign += '          <img src="/images/admin-remove_12px @1x.png" class="remove-admin remove2 GroupFlRight arfImg" onclick="makeMember(\'' + va.img + '\',\'' + va.fullname + '\',\'' + va.designation + '\',\'' + va.id + '\',\'' + roomid + '\')">';
	                	mldesign += '          <img src="/images/admin-add_12px @1x.png" class="add-admin add2 GroupFlRight arfImg"  onclick="makeAdmin(\'' + va.img + '\',\'' + va.fullname + '\',\'' + va.designation + '\',\'' + va.id + '\',\'' + roomid + '\')">';
	                }
	                mldesign += '        </div>';
	                $("#ml-listHl").append(mldesign);
	                $("#ml-membertype").show();
	            }
	        }
	    });

	    if (adminArra !== null) {
	        $.each(adminArra, function(kad, vad) {
	            if (va.id == vad) {
	                var mldesign = '<div class="member-div" id="member' + va.id + '">';
	                mldesign += '          <img src="/images/users/' + va.img + '" class="member-img">';
	                mldesign += '          <div class="member-name">' + va.fullname + '</div>';
	                // mldesign += '          <div class="member-designation">' + va.designation + '</div>';
	                if ($.inArray(user_id, adminArra) !== -1) {
	                	mldesign += '          <img src="/images/remove_8px_200 @1x.png" class="remove-it GroupFlRight" onclick = "removeMember(\'admin\',\'' + va.id + '\',\'' + roomid + '\');">';
	                	mldesign += '          <img src="/images/admin-remove_12px @1x.png" class="remove-admin remove2 GroupFlRight arfImg" onclick="makeMember(\'' + va.img + '\',\'' + va.fullname + '\',\'' + va.designation + '\',\'' + va.id + '\',\'' + roomid + '\')">';
	                	mldesign += '          <img src="/images/admin-add_12px @1x.png" class="add-admin add2 GroupFlRight arfImg"  onclick="makeAdmin(\'' + va.img + '\',\'' + va.fullname + '\',\'' + va.designation + '\',\'' + va.id + '\',\'' + roomid + '\')">';
	                }
	                mldesign += '        </div>';
	                $("#ml-listHA").append(mldesign);
	                $("#ml-admintype").show();
	            }
	        });
	    }

	   var definedList = '<li>';
			definedList += '      <img src="/images/users/'+va.img+'" class="profile">';
			definedList += '      <spna class="name s-l-def-clas" data-uuid="'+va.id+'">'+va.fullname+'</spna> <spna class="designation-name">'+va.designation+'</spna>';
			definedList += '    </li>';

		$("#s-l-def").append(definedList);
		$("#directMsgUserList").append(definedList);

	});



	$('#grpPrivacy').click(function(e) {
		e.stopImmediatePropagation();

		if($("#roomIdDiv").attr('data-rfu') == 'ready'){
			var roomid = $("#roomIdDiv").attr('data-roomid');
			var roomTitle = $("#roomIdDiv").attr('data-title');

			if ($.inArray(user_id, adminArra) !== -1) {
				if(e.target.checked){
					var grpprivacy = 'private';
				}else{
					var grpprivacy = 'public';
				}

				socket.emit('updatePrivecy', {
		            conversation_id: roomid,
		            grpprivacy: grpprivacy
		        }, (callBack) => {

		        	console.log(callBack);
		            toastr["success"]("This room is "+grpprivacy+" now", "Success");

		            if(grpprivacy == 'private'){
		            	$("#conv"+roomid).find('span').removeClass('hash').addClass('lock');
		            	$("#conv"+roomid).find('.unreadMsgCount').removeClass('lock');

		            }

		            if(grpprivacy == 'public'){
		            	$("#conv"+roomid).find('span').removeClass('lock').addClass('hash');
		            	$("#conv"+roomid).find('.unreadMsgCount').removeClass('hash');
		            }
		        });

			}else{
				toastr["warning"]("Please contact with room owner or admin", "Warning");
			}

		}
	});

	all_action_for_selected_member();
};

var roomFromJOin = (participantsOld, admin, roomid, title, privecy, keyspace) =>{

	$("#divCheck").val('2');

	if(participantsOld != null){
		participants = participantsOld.split(',');
	}

	if(admin != null){
		adminArra = admin.split(',');
	}else{
		adminArra = admin;
	}

	$(".connect_right_section").hide();
	$('#createChannelContainer').show();
	$('.memberList').show();
	$("#ml-listHl").html("");
	$("#ml-listHA").html("");
	$("#team-name").val(title);

	$(".submitBtn").hide();
	$(".create-channel-heading").text(title);

	// This line use for checking room update or room create
	$("#roomIdDiv").attr('data-rfu','ready');

	$("#select-ecosystem option").each(function(){
			if($(this).val()== keyspace){ // match here
				$(this).attr("selected","selected");
			}
		});

	if(privecy == 'private'){
		$('#grpPrivacy').attr('checked', true);
	}

	$(".add-team-member").prop("disabled",true);

	$.each(allUserdata[0].users, function(ky, va) {
		$.each(participants, function(k, v) {
			if (va.id === v) {
				if (jQuery.inArray(v, adminArra) === -1) {
					var mldesign = '<div class="member-div" id="member' + va.id + '">';
						mldesign += '          <img src="/images/users/' + va.img + '" class="member-img">';
						mldesign += '          <div class="member-name">' + va.fullname + '</div>';
						mldesign += '        </div>';
					$("#ml-listHl").append(mldesign);
					$("#ml-membertype").show();
				}
			}
		});

		if (adminArra !== null) {
			$.each(adminArra, function(kad, vad) {
				if (va.id == vad) {
					var mldesign = '<div class="member-div" id="member' + va.id + '">';
						mldesign += '          <img src="/images/users/' + va.img + '" class="member-img">';
						mldesign += '          <div class="member-name">' + va.fullname + '</div>';
						mldesign += '        </div>';
					$("#ml-listHA").append(mldesign);
					$("#ml-admintype").show();
				}
			});
		}
	});
};

var removeMember = (targetUser, targetID, conversation_id) => {

	if(targetUser == 'admin'){
		if(adminArra.length > 1){
			var remain = parseInt($("#conv"+conversation_id).attr('data-tm'))-1;
		    $.ajax({
		        type: 'POST',
		        data: {
		            conversation_id: conversation_id,
		            targetUser: targetUser,
		            targetID: targetID
		        },
		        dataType: 'json',
		        url: '/hayven/groupMemberDelete',
		        success: function(data) {
		            if (data == 'creator') {
		                toastr["warning"]("You can\'t delete this user", "Warning");
		            } else {
		                $("#member" + targetID).remove();
		                $("#conv"+conversation_id).attr('data-tm',remain);
		                removeA(adminArra,targetID);
		            }
		        }
		    });
		}else{
	    	toastr["warning"]("You can\'t remove this user. Make an admin first", "Warning");
	    }
	}else{
		var remain = parseInt($("#conv"+conversation_id).attr('data-tm'))-1;
	    $.ajax({
	        type: 'POST',
	        data: {
	            conversation_id: conversation_id,
	            targetUser: targetUser,
	            targetID: targetID
	        },
	        dataType: 'json',
	        url: '/hayven/groupMemberDelete',
	        success: function(data) {
	            if (data == 'creator') {
	                toastr["warning"]("You can\'t delete this user", "Warning");
	            } else {
	                $("#member" + targetID).remove();
	                $("#conv"+conversation_id).attr('data-tm',remain);
	            }
	        }
	    });
	}

};

// makeMember function used for make member as  a member from admin
var makeMember = (img, name, desig, id, cnvid) => {

    if(adminArra.length > 1){
    	$.ajax({
	        type: 'POST',
	        data: {
	            conversation_id: cnvid,
	            targetUser: name,
	            targetID: id
	        },
	        dataType: 'json',
	        url: '/hayven/makeMember',
	        success: function(data) {
	            $("#member" + id).remove();
	            var mldesign = ' <div class="member-div"  id="member' + id + '">';
		            mldesign += '   <img src="/images/users/' + img + '" class="member-img">';
		            mldesign += '   <div class="member-name">' + name + '</div>';
		            mldesign += '   <div class="member-designation" style="float:left;"></div>';
		            mldesign += '	<img src="/images/remove_8px_200 @1x.png" class="remove-it GroupFlRight" onclick = "removeMember(\'member\',\'' + id + '\',\'' + cnvid + '\');">';
		            mldesign += '   <img src="/images/admin-remove_12px @1x.png" class="remove-admin remove2 GroupFlRight arfImg" onclick="makeMember(\'' + img + '\',\'' + name + '\',\'' + desig + '\',\'' + id + '\',\'' + cnvid + '\')">';
		            mldesign += '   <img src="/images/admin-add_12px @1x.png" class="add-admin add2 GroupFlRight arfImg"   onclick="makeAdmin(\'' + img + '\',\'' + name + '\',\'' + desig + '\',\'' + id + '\',\'' + cnvid + '\')">';
		            mldesign += ' </div>';

	            $('.ml-listHl').append(mldesign);

	            $(".ml-listHl .member-div").mouseenter(function(e) {
	                $(this).find('.add-admin').show();
	                $(this).find('.remove-it').show();
	            }).mouseleave(function() {
	                $(this).find('.add-admin').hide();
	                $(this).find('.remove-it').hide();
	            });

	            if ($('#ml-listHA .member-div').length > 0) {
	                $("#ml-admintype").show();
	            } else {
	                $("#ml-admintype").hide();
	            }

	            if ($('#ml-listHl .member-div').length > 0) {
	                $("#ml-membertype").show();
	            } else {
	                $("#ml-membertype").hide();
	            }
	            removeA(adminArra,id);
	        }
	    });
    }else{
    	toastr["warning"]("You can\'t remove this user. Make an admin first", "Warning");
    }
};

// makeMember function used for make admin as  a member from member
var makeAdmin = (img, name, desig, id, cnvid) => {
    //alert(conversation_id);
    $.ajax({
        type: 'POST',
        data: {
            conversation_id: cnvid,
            targetUser: name,
            targetID: id
        },
        dataType: 'json',
        url: '/hayven/makeAdmin',
        success: function(data) {
            $("#member" + id).remove();
            var mldesign = ' <div class="member-div"  id="member' + id + '">';
                mldesign += '   <img src="/images/users/' + img + '" class="member-img">';
                mldesign += '   <div class="member-name">' + name + '</div>';
                mldesign += '   <div class="member-designation" style="float:left;"></div>';
                mldesign += '   <img src="/images/remove_8px_200 @1x.png" class="remove-it GroupFlRight" onclick = "removeMember(\'admin\',\'' + id + '\',\'' + cnvid + '\');">';
                mldesign += '   <img src="/images/admin-remove_12px @1x.png" class="remove-admin remove2 GroupFlRight arfImg" onclick="makeMember(\'' + img + '\',\'' + name + '\',\'' + desig + '\',\'' + id + '\',\'' + cnvid + '\')">';
                mldesign += '   <img src="/images/admin-add_12px @1x.png" class="add-admin add2 GroupFlRight arfImg"   onclick="makeAdmin(\'' + img + '\',\'' + name + '\',\'' + desig + '\',\'' + id + '\',\'' + cnvid + '\')">';
                mldesign += ' </div>';

            $('.ml-listHA').append(mldesign);

            $(".ml-listHA .member-div").mouseenter(function(e) {
                $(this).find('.remove-admin').show();
                $(this).find('.remove-it').show();
            }).mouseleave(function() {
                $(this).find('.remove-admin').hide();
                $(this).find('.remove-it').hide();
            });

            if ($('#ml-listHA .member-div').length > 0) {
                $("#ml-admintype").show();
            } else {
                $("#ml-admintype").hide();
            }

            if ($('#ml-listHl .member-div').length > 0) {
                $("#ml-membertype").show();
            } else {
                $("#ml-membertype").hide();
            }

            adminArra.push(id);
        }
    });
};

$("#team-name").on('blur keypress', function(e) {
    var code = e.keyCode || e.which;
    if($("#roomIdDiv").attr('data-rfu') == 'ready'){
    	if ($.inArray(user_id, adminArra) !== -1) {
    		var roomid = $("#roomIdDiv").attr('data-roomid');
			var roomTitle = $("#roomIdDiv").attr('data-title');
			if (code == 13) { //Enter keycode = 13
		        e.preventDefault();
		        if ($("#team-name").val() != "") {
		            var newGroupname = $("#team-name").val();
		            socket.emit('saveGroupName', {
		                conversation_id: roomid,
		                newGroupname: newGroupname
		            }, (callBack) => {

		            	console.log(callBack);

		                if (callBack.status) {
		                    $("#team-name").focusout() ;
		                    $("#conv_title").text("#"+newGroupname) ;
		                    $("#conv"+roomid).html('<span class="hash" style="left: 12px;"></span>'+newGroupname) ;
		                    $("#roomIdDiv").attr('data-title',newGroupname)
		                }
		            });
		        } else {
		            $("#team-name").val(groupName);
		            toastr["warning"]("You can\'t set an empty group name", "Warning");
		        }
		    }
		}else{
			toastr["warning"]("Please contact with room owner or admin", "Warning");
		}

	}
});

var updateWorkspace = (thisValue)=>{
	if($("#roomIdDiv").attr('data-rfu') == 'ready'){
		if ($.inArray(user_id, adminArra) !== -1) {

			var roomid = $("#roomIdDiv").attr('data-roomid');
			var roomTitle = $("#roomIdDiv").attr('data-title');

			socket.emit('updateKeySpace', {
	            conversation_id: roomid,
	            keySpace: thisValue
	        }, (callBack) => {

	        	console.log(callBack);
	            toastr["success"]("Workspace changed successfully", "Success");
	        });
		}else{
			toastr["warning"]("Please contact with room owner or admin", "Warning");
		}
	}
};

// msg history for own msg
var tagListp = [];
var my_conversation = (event) =>{

	adminArra = [];
	participants = [];

	if($('#groupChatContainer').is(":visible") == false){
		$(".connect_right_section").hide();
		$('#groupChatContainer').show();
	}

	closeRightSection();
	var id = to = room_id = $(event.target).attr("data-id");
	var type = conversation_type = $(event.target).attr("data-conversationtype");
	var conversationid = conversation_id = $(event.target).attr("data-conversationid");
	var name = room_name = $(event.target).attr("data-name");
	var img = room_img = $(event.target).attr("data-img");
	var subtitle = $(event.target).attr("data-subtitle");
	var tm = $(event.target).attr("data-tm");

	$("#pin-to-bar").attr('data-conversationid','');
	$("#createConvTag").attr('data-roomid',conversationid);

	$("#pin-to-bar").attr('data-id','');
	$("#pin-to-bar").attr('data-subtitle','');
	$("#pin-to-bar").attr('data-img','');
	$("#pin-to-bar").attr('data-name','');
	$("#pin-to-bar").attr('data-type','');
	$("#pin-to-bar").attr('src','/images/basicAssets/custom_pinned.svg');

	//use for set title upper side of msg body
	$("#conv_title").text('#'+name);
	$("#conv_key").text('@'+subtitle);
	$("#totalMember").text('1');

	$("#totalMember").text(tm);
	// console.log({type, id, conversationid, name, img});
	$("#msg").html("");
	$("#msg-container").html("");

	// For tag purpose. while clicking on room or personal
	$('.chat-head-calling .addTagConv').hide();
	$('.chat-head-calling .tagged').show();
	$("#taggedList").html("");
	$("#levelListp").html("");


	var this_msg_unread = $("#conv"+conversation_id).find(".unreadMsgCount").html();
	total_unread_count -= Number(this_msg_unread);
	display_show_hide_unread_bar(total_unread_count);
	$("#conv"+conversation_id).find(".unreadMsgCount").html("");
	socket.emit('get_conversation_history', {type, id, conversationid, name, img,user_id}, (respons) =>{

		if(respons.tags != undefined){
			$.each(respons.tags, function(k,v){
				var design ='<li>'+v.title+'</li>';
				var level ='<p>'+v.title+'</p>';
                $('#taggedList').append(design);
                tagListp.push(v.title);
			});
			$("#createConvTag").val(tagListp.join(','));

		}

		$.each(respons.conversation_list, function(k,v){
			draw_msg(v);
			if (v.msg_status == null) {
		        if (v.sender == user_id) {
		            // This msg send by this user; so no need to change any seen status
		        } else {
		            // This msg receive by this user; so need to change seen status
		            need_update_message_seen_list.push(v.msg_id);
		        }
		    }

		    // If msg status have some user id, then
		    else {
		        if (v.msg_status.indexOf(user_id) > -1) {
		            // This msg already this user seen
		        } else {
		            if (v.sender != user_id) {
		                // This msg receive by this user; so need to change seen status
		                need_update_message_seen_list.push(v.msg_id);
		            }
		        }
		    }
		});
		scrollToBottom('.chat-page');
		if (need_update_message_seen_list.length > 0) {
            $.ajax({
                url: '/hayven/update_msg_status',
                type: 'POST',
                data: {
                    msgid_lists: JSON.stringify(need_update_message_seen_list),
                    user_id: user_id
                },
                dataType: 'JSON',
                success: function(res) {
                    socket.emit('update_msg_seen', {
                        msgid: need_update_message_seen_list,
                        senderid: to,
                        receiverid: user_id,
                        conversation_id: conversation_id
                    });
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
	});
	$('.side_bar_list_item li').css('border-left', 'none');
	$('.side_bar_list_item li').css('background', 'transparent');
	$('.side_bar_list_item li').css('padding-left', '42px');
	$('.side_bar_list_item li').children(".hash, .online, .offline, .lock").css('left', '12px');
	$(event.target).css('border-left', '6px solid #1676EA');
	$(event.target).css('background', 'rgba(22, 118, 234, 0.08)');
	$(event.target).css('padding-left', '36px');
	$(event.target).children(".hash, .online, .offline, .lock").css('left', '6px');
	unread_msg_conv_intop();
};

$("#createConvTag").on('blur keypress', function(e) {
    var code = e.keyCode || e.which;
	if (code == 13) { //Enter keycode = 13
		var roomid = $("#createConvTag").attr('data-roomid');
		var tagTitle = $("#createConvTag").val();
        e.preventDefault();
        if(tagTitle != "" ){
        	if(roomid == ""){
	        	toastr["warning"]("You have to select a room or personal conversation", "Warning");
	        	$(this).val("");
	        }else{


	        	var tagArr = tagTitle.split(',');
						var sendTagarr = [];
						var pTag = [];

						$.each(tagArr, function(k,v){
	        		if(tagListp.indexOf(v.toLowerCase()) === -1){
	        			sendTagarr.push(v);
	        			tagListp.push(v.toLowerCase());
	        		}
	        	});

						if(sendTagarr.length>0){
							socket.emit('saveTag', {
					        	created_by: user_id,
					            conversation_id: roomid,
					            tagTitle: sendTagarr
					        }, (callBack) => {
								if (callBack.status) {
									$.each(sendTagarr, function(k,v){
						        		var design ='<li>'+v+'</li>';
					                	$('.taggedList').append(design);
						        	});
								}else{
					            	if (callBack.err == 'at') {
					            		toastr["warning"]("\""+tagTitle+"\" already tagged", "Warning");
					            	}
					            }
					        });
						}else{
							toastr["warning"]("Alredy tagged", "Warning");
						}

			    }
        }else{
        	$("#createConvTag").focus();
        }
	}
});

$('#createConvTag').blur(function(){
	$('.chat-head-calling .addTagConv').hide();
	$('.chat-head-calling .tagged').show();
	//$(this).val("");
});
