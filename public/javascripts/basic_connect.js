
/* 770 */

var all_current_sms = [];
var to = "";
var conversation_id = "";
var conversation_type = "";
var room_id = "";
var room_name = "";
var room_img = "";
var filedata = [],
	audiofile = [],
	videofile = [],
	imgfile = [],
	otherfile = [];
var thread_id = "";
var thread_root_id = "";
var swap_conversation_id = "";
var need_update_message_seen_list = [];
var adminArra = [];
var participants = [];
var roomTitle = "";
var privacy = "";
var keySpace = "";

var scrollToBottom = (target) => {
  $('.chat-page').animate({ scrollTop: $(target).prop("scrollHeight") }, 800);
};

var unread_msg_conv_intop =()=>{
	$.each($(".side_bar_list_item li"), function(){
		if(!$(this).find(".unreadMsgCount").html() == ""){
			$(this).css('color', 'rgba(0,0,0,0.88)');
		}
	});

	var unread = ""; var read = ""; var count = 0;
	$.each($("#directListUl li"), function(k, v){
		if(count === 0) {
			count++;
			unread += v.outerHTML;
		}
		else{
			if(!$(v).find(".unreadMsgCount").html() == ""){
				unread += v.outerHTML;
			}else{
				read += v.outerHTML;
			}
		}
	});
	$("#directListUl").html(unread);
	$("#directListUl").append(read);
}
unread_msg_conv_intop();

var start_conversation = (event) =>{

	adminArra = [];
	participants = [];

	if($('#groupChatContainer').is(":visible") == false){
		$(".connect_right_section").hide();
		$('#groupChatContainer').show();
	}

	closeRightSection();

	var id = to = room_id = $(event.target).attr("data-id");
	var type = conversation_type = $(event.target).attr("data-conversationtype");
	var conversationid = conversation_id = $(event.target).attr("data-conversationid");
	var name = room_name = $(event.target).attr("data-name");
	var img = room_img = $(event.target).attr("data-img");
	var subtitle = $(event.target).attr("data-subtitle");
	var tm = $(event.target).attr("data-tm");

	$("#pin-to-bar").attr('data-conversationid',conversationid);
	$("#createConvTag").attr('data-roomid',conversationid);

	$("#pin-to-bar").attr('data-id',id);
	$("#pin-to-bar").attr('data-subtitle',subtitle);
	$("#pin-to-bar").attr('data-img',img);
	$("#pin-to-bar").attr('data-name',name);
	$("#pin-to-bar").attr('data-type',type);

	$("#conv_title").text('#'+name);
	$("#conv_key").text('@'+subtitle);
	$("#totalMember").text(tm);
	// console.log({type, id, conversationid, name, img});
	$("#msg").html("");
	$("#msg-container").html("");


	// For tag purpose. while clicking on room or personal
	$('.chat-head-calling .addTagConv').hide();
	$('.chat-head-calling .tagged').show();
	$("#taggedList").html("");

	//Msg placeholder
	$("#msg").attr('placeholder', 'Message '+name+'');

	// Chat head member count div
	if(type == "group"){
		$('.chat-head-name h4').css('display', 'block');
		$('.chat-head-name').css('margin-top', '17px');

	}else if(type == "personal")
	{
		$('.chat-head-name h4').css('display', 'none');
		$('.chat-head-name').css('margin-top', '28px');
	}

	tagListp = [];

	var this_msg_unread = $("#conv"+conversation_id).find(".unreadMsgCount").html();
	total_unread_count -= Number(this_msg_unread);
	display_show_hide_unread_bar(total_unread_count);
	$("#conv"+conversation_id).find(".unreadMsgCount").html("");
	socket.emit('get_conversation_history', {type, id, conversationid, name, img,user_id}, (respons) =>{

		if(respons.tags != undefined){
			$.each(respons.tags, function(k,v){
				var design ='<li>'+v.title+'</li>';
				var level ='<p>'+v.title+'</p>';
                $('#taggedList').append(design);
                tagListp.push(v.title.toLowerCase());
			});
			$("#createConvTag").val(tagListp.join(','));

		}

		if($.inArray(user_id, respons.conversation["0"].participants_admin) !== -1){
			adminArra = respons.conversation["0"].participants_admin;
			participants = respons.conversation["0"].participants;
			// console.log(84,{participants,adminArra});
			$("#roomIdDiv").attr('data-roomid',conversationid);
			$("#roomIdDiv").attr('data-title',respons.conversation["0"].title);
			$("#roomIdDiv").attr('data-privecy',respons.conversation["0"].privacy);
			$("#roomIdDiv").attr('data-keyspace',respons.conversation["0"].group_keyspace);
      		$("#roomIdDiv").attr('data-convimg',respons.conversation["0"].conv_img);

		}else{
			// $("#roomIdDiv").attr('data-roomid','0');
			adminArra = respons.conversation["0"].participants_admin;
			participants = respons.conversation["0"].participants;
			// console.log(94,{participants,adminArra});
			$("#roomIdDiv").attr('data-roomid',conversationid);
			$("#roomIdDiv").attr('data-title',respons.conversation["0"].title);
			$("#roomIdDiv").attr('data-privecy',respons.conversation["0"].privacy);
			$("#roomIdDiv").attr('data-keyspace',respons.conversation["0"].group_keyspace);
      		$("#roomIdDiv").attr('data-convimg',respons.conversation["0"].conv_img);
		}

		if(respons.pinnedStatus != undefined){
			$("#pin-to-bar").addClass('pined');
			$("#pin-to-bar").attr('data-pinned',respons.pinnedStatus.id);
			$("#pin-to-bar").attr('src','/images/basicAssets/custom_pinned.svg');
		}else{
			$("#pin-to-bar").removeClass('pined');
			$("#pin-to-bar").attr('data-pinned','');
			$("#pin-to-bar").attr('src','/images/basicAssets/custom_not_pin.svg');
		}

		$.each(respons.conversation_list, function(k,v){
			draw_msg(v);
			if (v.msg_status == null) {
		        if (v.sender == user_id) {
		            // This msg send by this user; so no need to change any seen status
		        } else {
		            // This msg receive by this user; so need to change seen status
		            need_update_message_seen_list.push(v.msg_id);
		        }
		    }

		    // If msg status have some user id, then
		    else {
		        if (v.msg_status.indexOf(user_id) > -1) {
		            // This msg already this user seen
		        } else {
		            if (v.sender != user_id) {
		                // This msg receive by this user; so need to change seen status
		                need_update_message_seen_list.push(v.msg_id);
		            }
		        }
		    }
		});
		scrollToBottom('.chat-page');
		if (need_update_message_seen_list.length > 0) {
            $.ajax({
                url: '/hayven/update_msg_status',
                type: 'POST',
                data: {
                    msgid_lists: JSON.stringify(need_update_message_seen_list),
                    user_id: user_id
                },
                dataType: 'JSON',
                success: function(res) {
                    socket.emit('update_msg_seen', {
                        msgid: need_update_message_seen_list,
                        senderid: to,
                        receiverid: user_id,
                        conversation_id: conversation_id
                    });
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
	});
	sideBarActiveInactive(event);
	unread_msg_conv_intop();
};

var tooltipForOverLength =()=>{
	$('.side_bar_list_item li.over_length').mouseleave(function(){
		$('.side_bar_list_item').find('.tooltip_for_sidebar').remove();
	});

	$('.side_bar_list_item li.over_length').mouseenter(function(){
		var name = $(this).attr('data-name');
		var design = '<div class="tooltip_for_sidebar"><p>'+name+'</p><span></span></div>';
		$(this).after(design);
	});
}
tooltipForOverLength();

var draw_msg = (data) =>{
	/* Start Date Group By */
	var msg_date = moment(data.created_at).calendar(null, {
	        sameDay: '[Today]',
	        lastDay: '[Yesterday]',
	        lastWeek: function(now) {return '['+this.format("MMM Do, YYYY")+']';},
	        sameElse: function(now) {return '['+this.format("MMM Do, YYYY")+']';}
	     });

	$.each($('.msg-separetor'), function(k, v) {
		if ($(v).text() == msg_date) {
			msg_date = null;
			return 0;
		}
	});
	if(msg_date !== null){
		var date_html = '<div class="msg-separetor" data-date=""><p>'+ msg_date +'</p></div>';
		$("#msg-container").append(date_html);
	}
	/* End Date Group By */


	var html = 	'<div class="msgs-form-users msg_id_'+ data.msg_id +'" data-msgid="'+ data.msg_id +'">';
		html +=		'<div class="msg-user-photo">';
		html +=			'<img src="/images/users/'+ data.sender_img +'" alt="">';
		html +=		'</div>';
		html +=		'<div class="user-msg">';
		html +=			'<h4>'+ data.sender_name;
		html +=				'&nbsp;<span class="msg-time">'+ moment(data.created_at).format('h:mm a') +'</span>';
		// Check flag and unflag message
	    if(data.has_flagged != null && (data.has_flagged).indexOf(user_id) != -1){
	   						html += '&nbsp;<img class="flaggedMsg" src="/images/basicAssets/Flagged.svg">';
	    }
		html +=			'</h4>';
		html +=			'<p>'+ data.msg_body +'</p>';
		if(data.has_reply > 0){
			html += per_msg_rep_btn(data.has_reply);
		}
		if(data.attch_videofile!==null){
			html += per_msg_video_attachment(data.attch_videofile);
		}
		if(data.attch_imgfile!==null){
			html += per_msg_img_attachment(data.attch_imgfile, data.sender_name, data.sender_img);
		}
		if(data.attch_audiofile!==null){
			html += per_msg_audio_attachment(data.attch_audiofile);
		}
		if(data.attch_otherfile!==null){
			html += per_msg_file_attachment(data.attch_otherfile);
		}
		html +=			'<div class="replies">';
		// Check emoji reaction message
		if(data.has_emoji !== null){
			$.each(data.has_emoji, function(k, v){
				if(v>0)
					html += emoji_html(k, "/images/emoji/"+ k +".png", v);
			});
		}
		html +=			'</div>';
		html +=		'</div>';
		html +=		'<div class="msgs-form-users-options">';
		html +=			'<div class="call-rep-emoji" onclick="viewEmojiList(event)"><img src="/images/basicAssets/AddEmoji.svg" alt=""></div>';
		// Check flag and unflag message
	    if(data.has_flagged != null && (data.has_flagged).indexOf(user_id) != -1){
	   					html += '<div class="flag" onclick="flggUserMsg(event)"><img src="/images/basicAssets/Flagged.svg" alt=""></div>';
	    }
		else{
						html +=	'<div class="flag" onclick="flggUserMsg(event)"><img src="/images/basicAssets/NotFlagged.svg" alt=""></div>';
		}
		html +=			'<div class="replys" onclick="threadReply(event)"><img src="/images/basicAssets/Thread.svg" alt=""></div>';
		html +=			'<div class="more">';
		html +=				'<img src="/images/basicAssets/MoreMenu.svg" alt="">';
		html +=				'<div class="msg-more-popup" style="display:none">';
		html +=					'<p onclick="viewCreateTodoPopup()">Create a to do</p>';
		html +=					'<p>Schedule an event</p>';
		html +=					'<p>Start a poll</p>';
		html +=					'<p>Share Message</p>';
		html +=				'</div>';
		html +=			'</div>';
		html +=		'</div>';
		html +=	'</div>';
	$("#msg-container").append(html);

	moreMsgAction();
	viewThread();
};
var per_msg_img_attachment = (msg_attach_img, sender_name, sender_img) => {
	var html = "";
	var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,width=600,height=400";
	$.each(msg_attach_img, function(k,v){
		html +=	'<img data-sender_name="'+ sender_name +'" data-sender_img="'+ sender_img +'" class="img_attach" src="'+ file_server +'/upload/'+ v +'" alt="'+ v +'" onclick="window.open(\''+ file_server +'/upload/'+ v +'\', \'Image Viewer\', \''+ strWindowFeatures +'\')">';
	});
	return html;
}
var per_msg_video_attachment = (msg_attach_video) => {
	var html = "";
	$.each(msg_attach_video, function(k, v) {
		var file_type = v.split('.').pop().toLowerCase();
		html += '<video controls class="media-msg">';
		html += '<source class="vdo_attach" src="' + file_server + '/upload/' + v + '" type="video/' + file_type + '">';
		html += 'Your browser does not support HTML5 video.';
		html += '</video>';
	});
	return html;
}
var per_msg_audio_attachment = (msg_attach_audio) => {
  var html = "";
  $.each(msg_attach_audio, function(k, v) {
    var file_type = v.split('.').pop().toLowerCase();
    html += '<audio controls class="media-msg">';
    html += '<source class="ado_attach" src="' + file_server + '/upload/' + v + '" type="audio/' + file_type + '">';
    html += 'Your browser does not support audio tag.';
    html += '</audio>';
  });
  return html;
}

var per_msg_file_attachment = (msg_attach_file) => {
  var html = "";
  $.each(msg_attach_file, function(k, v) {
    var file_type = v.split('.').pop().toLowerCase();
    switch (file_type) {
      case 'ai':
      case 'mp3':
      case 'doc':
      case 'docx':
      case 'indd':
      case 'js':
      case 'sql':
      case 'pdf':
      case 'ppt':
      case 'pptx':
      case 'psd':
      case 'svg':
      case 'xls':
      case 'xlsx':
      case 'zip':
      case 'rar':
      ext = file_type;
      break;
      default:
      ext = 'other';
    }
    html += '<a href="' + file_server + '/upload/' + v + '" target="_blank">';
    html += '<div class="fil_attach attach-file lightbox" data-filetype="' + ext + '" data-src="' + file_server + '/upload/' + v + '">';
    html += '<img src="/images/file_icon/' + ext + '.png" alt="' + v + '">';
    html += '<div class="file-name">' + v.substring(0, v.lastIndexOf('@')) + '.' + file_type + '</div>';
    html += '<div class="file-time">' + moment().format('h:mm a') + '</div>';
    html += '</div>';
    html += '</a>';
    // console.log(html);
  });
  return html;
};
var per_msg_rep_btn = (count) =>{
	var html = "";
	html += 		'<div class="msgReply" onclick="threadReply(event)">';
	html +=				'<div class="groupImg">';
	for(var i=0; i<count; i++)
		html +=				'<img src="/images/users/img.png">';
	html +=				'</div>';
	html +=				'<div class="countReply">';
	html += 				'<img src="/images/basicAssets/custom_thread_for_reply.svg">';
	html += 				'<p><span class="no-of-replies">'+ count +'</span> Reply </p>';
	html +=					'<img class="replyarrow" src="/images/basicAssets/custom_rightChevron_for_reply.svg">';
	html +=				'</div>';
	html += 		'</div>';
	return html;
}
/* Start emoji sending */
var emoji_div_draw = () => {
  var design = '<div class="emoji_div">';
	  design += '<div class="emoji-header"><img src="/images/emoji/temp-emoji-head.png"></div>';
	  design += '<div class="search-emoji-from-list">';
	  design += '<input type="text" placeholder="Search">';
	  design += '</div>';
	  design += '<div class="emoji-container-name">SMILEYS & PEOPLE</div>';
	  design += '<div class="emoji-container">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/rage.png">';
	  design += '<img src="/images/emoji/heart.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/heart.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/heart.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/heart.png">';
	  design += '<img src="/images/emoji/grinning.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '<img src="/images/emoji/disappointed_relieved.png">';
	  design += '<img src="/images/emoji/joy.png">';
	  design += '<img src="/images/emoji/open_mouth.png">';
	  design += '</div>';
  design += '</div>';
  return design;
};
var open_emoji = () => {
  if ($('.emoji_div').length == 0) {
    var design = emoji_div_draw();
    $('.send-msgs').append(design);
    insert_emoji();
  } else {
    $('.emoji_div').remove();
    // $('.backWrap').css('background-color', 'rgba(0, 0, 0, 0.33)');
    // $('.backWrap').hide();
  }
}
var insert_emoji = () => {
  $('.emoji_div .emoji-container>img').on('click', function() {
    var emoji_name = $(this).attr('src');
    $('#msg').append('<img src="' + emoji_name + '" style="width:20px; height:20px; vertical-align: middle;" />&nbsp;');
    open_emoji();
    var el = document.getElementById("msg");
	placeCaretAtEnd(el);
  });
};

var placeCaretAtEnd = (el) => {
    el.focus();
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
};
/* End emoji sending */
/**
* When message form submit
**/
var convert = (str) => {
  var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  var text1 = str.replace(exp, "<a target='_blank' href='$1'>$1</a>");
  var exp2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
  return text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
};

var msg_form_submit = () => {
  var str = $('#msg').html();
  str = str.replace(/(<\/?(?:img|br)[^>]*>)|<[^>]+>/ig, '$1'); // replace all html tag
  str = convert(str);
  str = str.replace(/&nbsp;/gi, '').trim();
  str = str.replace(/^(\s*<br( \/)?>)*|(<br( \/)?>\s*)*$/gm, '');
  if (str != "") {
    msg_sending_process(str);
  }
};

var draw_typing_indicator = (add_remove, img, name) => {
  if (add_remove) {
    if ($('.typing-indicator').html() == "") {
      $('.typing-indicator').html(name + '&nbsp;<span>is typing....</span>');
    }
  } else {
    $('.typing-indicator').html("");
  }
};
/* End of 770 */



function createChannel(){
	$(".create-channel-heading").text("Create Room");
	$(".connect_right_section").hide();
	$('#createChannelContainer').show();

	if(!$("#defaultRoom").is(":visible")){
		$("#channelList").prepend('<li id="defaultRoom"><span class="hash"></span> Default</li>');
	}

	$(".add-team-member").prop("disabled",false);
	$("#ml-admintype").hide();
	$("#ml-membertype").hide();
	$("#ml-listHA").html('');
	$("#ml-listHl").html('');
	$("#team-name").val('');
	$("#select-ecosystem option").prop("selected", false);
	$('#grpPrivacy').attr('checked', false);

	adminArra = [];
	participants = [];

	$("#roomIdDiv").attr('data-roomid','');
	$("#roomIdDiv").attr('data-title','');
	$("#roomIdDiv").attr('data-privecy','');
	$("#roomIdDiv").attr('data-keyspace','');
	$("#roomIdDiv").attr('data-rfu','');
	$(".submitBtn").show();

  	$("#demoImg").attr('src','/images/basic_view/channel-photo.JPG');
	$("#upload-channel-photo").attr("onchange","getInfo(event)");

	$("#s-l-def").html("");
	$("#directMsgUserList").html("");

	$.each(allUserdata[0].users, function(ky,va){
		var definedList = '<li>';
			definedList += '      <img src="/images/users/'+va.img+'" class="profile">';
			definedList += '      <spna class="name s-l-def-clas" data-uuid="'+va.id+'">'+va.fullname+'</spna> <spna class="designation-name">'+va.designation+'</spna>';
			definedList += '    </li>';

		$("#s-l-def").append(definedList);
		$("#directMsgUserList").append(definedList);
	});

	all_action_for_selected_member();

}

function createDirMsg(){
	$(".connect_right_section").hide();
	$('#createDirMsgContainer').show();

	$("#ml-admintype").hide();
	$("#ml-membertype").hide();
	$(".ml-listHA").html('');
	$(".ml-listHl").html('');

	$("#s-l-def").html("");
	$("#directMsgUserList").html("");

	$.each(allUserdata[0].users, function(ky,va){
		var definedList = '<li>';
			definedList += '      <img src="/images/users/'+va.img+'" class="profile">';
			definedList += '      <spna class="name s-l-def-clas" data-uuid="'+va.id+'">'+va.fullname+'</spna> <spna class="designation-name">'+va.designation+'</spna>';
			definedList += '    </li>';

		$("#s-l-def").append(definedList);
		$("#directMsgUserList").append(definedList);
	});

	all_action_for_selected_member();
}


function closeRightSection(){

	if($("#divCheck").val() == '1'){
		if($("#createChannelContainer").is(":visible") && $("#defaultRoom").is(":visible")){
			$("#defaultRoom").remove();
		}

		$(".connect_right_section").hide();
		$('#groupChatContainer').show();

		$(".add-team-member").prop("disabled",false);
		$('#grpPrivacy').prop("disabled",false);
		$(".add-direct-member").val('');
		$(".add-team-member").val('');

		memberList = [];
		memberListUUID = [];
		directMsgCont = 1;

		$("#ml-admintype").hide();
		$("#ml-membertype").hide();

		$(".ml-listHA").html('');
		$(".ml-listHl").html('');
		$("#team-name").val('');
		$("#select-ecosystem option").prop("selected", false);
		$('#grpPrivacy').attr('checked', false);

		$("#roomIdDiv").attr('data-roomid','');
		$("#roomIdDiv").attr('data-title','');
		$("#roomIdDiv").attr('data-privecy','');
		$("#roomIdDiv").attr('data-keyspace','');
		$("#roomIdDiv").attr('data-rfu','');
		$(".submitBtn").show();
	}else if($("#divCheck").val() == '2'){
		joinChannelPanel();
		$("#divCheck").val('1');
	}


}

function createToDo(e){
	if($('.create-todo-popup').is(":visible") == false){
		$('.create-todo-popup').show();
		$('#createTodo').hide();
		$('.create-todo-popup-title').focus();
	}
}
function createEventPop(e){
	if($('.create-event-popup').is(":visible") == false){
		$('.create-event-popup').show();
		$('#CreateEvent').hide();
		$('.create-event-popup-title').focus();
	}
}
function searchConversation(e){
	if($('.search-panel').is(":visible") == false){
		$('.search-panel').show();
		$('#searchConversation').hide();
		$('.search-panel input').focus();
	}
}
function eventToggleAdvance(){
	if($('#eventAdvanceOption').is(":visible") == false){
		$('#eventAdvanceOption').show();
		$('.channel-member-search input').focus();
	}else{
		$('#eventAdvanceOption').hide();
	}
}
function todoToggleAdvance(){
	if($("#todoAdvanceOption").is(":visible") == true){
		$("#todoAdvanceOption").hide();
	}else{
		$("#todoAdvanceOption").show();
		$('.channel-member-search input').focus();
	}
}
function searchFilter(){
	if($('.filterMainContainer').is(":visible") == false){
		$('.filterMainContainer').show();
		$('.side-bar-filter-icon').addClass('active');
	}
}
function chooseTag(){
	if($('.filterMainContainer').is(":visible") == true){
		$('.chooseTag').show();
	}
}
function moreMenuPopup() {
	if($('.moreMenuPopup').is(":visible") == false){
		$('.moreMenuPopup').show();
	}
}

function joinChannelPanel() {

	var keySpace = 'Navigate';
	socket.emit('public_conversation_history', {keySpace}, (respons) =>{
		if(respons.staus){
			$("#publicRoomsList").html("");
			$(".connect_right_section").hide();
			$('#joinChannelPanel').show();
			$.each(respons.rooms, function(k,v){

				var ststus = (v.privacy == "public" ? "hash":"lock");
				if($.inArray(user_id, v.participants) !== -1){
					var totalMember = v.participants;
					var roomDesign =  '<div class="added-channels">';
				        roomDesign += '		<div class="channel-joined" id="roomJoin'+v.conversation_id+'" style="display:blockrr;"><img src="/images/basicAssets/joined.png" alt="">Joined</div>';
				        roomDesign += '		<h3 class="chanel-name" id="joinChanelTile'+v.conversation_id+'" data-roomid="'+v.conversation_id+'" data-rfu="join" data-title="'+v.title+'" data-privecy="'+v.privacy+'" data-keyspace="'+v.group_keyspace+'" data-participants="'+v.participants+'" data-admin="'+v.participants_admin+'" onclick="roomFromJOin($(this).attr(\'data-participants\'),$(this).attr(\'data-admin\'),$(this).attr(\'data-roomid\'),$(this).attr(\'data-title\'),$(this).attr(\'data-privecy\'),$(this).attr(\'data-keyspace\'))"><span class="'+ststus+'"></span><span id="roomTitle'+v.conversation_id+'">'+v.title+'<span></h3>';
				        roomDesign += '		<p class="channel-members"><img src="/images/basicAssets/Users.svg" alt="">'+totalMember.length+' Members</p>';
				        roomDesign += '		<div class="channel-tags">';
				        $.each(respons.convTag, function(ck,cv){
				        	if(cv.cnvID == v.conversation_id){
				        		roomDesign += '  		<p>'+cv.title+'</p>';
				        	}
				        });
				        roomDesign += '		</div>';
				        if($.inArray(user_id, v.participants_admin) !== -1){
							roomDesign += '		<h3 class="click-to-leave" id="roomBtn'+v.conversation_id+'">Leave Room</h3>';
						}else{
							roomDesign += '		<h3 class="click-to-leave" id="roomBtn'+v.conversation_id+'" onclick="leaveRoom(\''+v.conversation_id+'\',\''+user_id+'\')">Leave Room</h3>';
						}

				      	roomDesign += '</div>';

				    $("#publicRoomsList").append(roomDesign);
				}else{
					var totalMember = v.participants;
					var roomDesign =  '<div class="added-channels">';
				        roomDesign += '		<div class="channel-joined" id="roomJoin'+v.conversation_id+'" style="display:none;"><img src="/images/basicAssets/joined.png" alt="">Joined</div>';
				        roomDesign += '		<h3 class="chanel-name" id="joinChanelTile'+v.conversation_id+'" data-roomid="'+v.conversation_id+'" data-rfu="join" data-title="'+v.title+'" data-privecy="'+v.privacy+'" data-keyspace="'+v.group_keyspace+'" data-participants="'+v.participants+'" data-admin="'+v.participants_admin+'" onclick="roomFromJOin($(this).attr(\'data-participants\'),$(this).attr(\'data-admin\'),$(this).attr(\'data-roomid\'),$(this).attr(\'data-title\'),$(this).attr(\'data-privecy\'),$(this).attr(\'data-keyspace\'))"><span class="'+ststus+'"></span><span id="roomTitle'+v.conversation_id+'">'+v.title+'<span></h3>';
				        roomDesign += '		<p class="channel-members"><img src="/images/basicAssets/Users.svg" alt="">'+totalMember.length+' Members</p>';
				        roomDesign += '		<div class="channel-tags">';
				        $.each(respons.convTag, function(ck,cv){
				        	if(cv.cnvID == v.conversation_id){
				        		roomDesign += '  		<p>'+cv.title+'</p>';
				        	}
				        });
				        roomDesign += '		</div>';
				        if($.inArray(user_id, v.participants_admin) !== -1){
							roomDesign += '		<h3 class="click-to-join" id="roomBtn'+v.conversation_id+'">Join Room</h3>';
						}else{
							roomDesign += '		<h3 class="click-to-join" id="roomBtn'+v.conversation_id+'" onclick="joinRoom(\''+v.conversation_id+'\',\''+user_id+'\',\''+v.title+'\')">Join Room</h3>';
						}

				    	roomDesign += '</div>';
				    $("#publicRoomsList").append(roomDesign);
				}
			});
		}
	});
}
// function moreMenuPopup() {
// 		// $(e.this).children('.msg-more-popup').toggle();
// 		console.log(this);
// }

var moreMsgAction = ()=>{
	$('.more').on('click', function() {
		$(this).children('.msg-more-popup').show();
		var chat_Page_height = $('.chat-page').height();
		var scrollY = event.pageY;
		var total = chat_Page_height - event.pageY;
		if(total < 35){
			$(this).children('.msg-more-popup').css('top', '-195px')
		}
	});
	$('.msgs-form-users').mouseenter(function(){
		if($(this).height() > 60){
			$(this).children('.msgs-form-users-options').css('top', '16px');
		}
	});
}
moreMsgAction();
$(document).mouseup(function(e){
    var createToDoPop = $(".create-todo-popup");
    var createEventPop = $('.create-event-popup');
    var searchPanel = $('.search-panel');
    var filterPannel = $('.filterMainContainer');
	var moreMenuPopup = $('.moreMenuPopup');
	var moreMenumsgPopup = $('.msg-more-popup');
	var repEmojiDiv = $('.emojiListContainer');

    // if the target of the click isn't the container nor a descendant of the container
    if (!createToDoPop.is(e.target) && createToDoPop.has(e.target).length === 0)
    {
        createToDoPop.hide();
        $('#createTodo').show();
    }
    if (!createEventPop.is(e.target) && createEventPop.has(e.target).length === 0)
    {

        if($("#calenderPicker").is(":visible") == true){
        	$("#calenderPicker").hide()
        }else{
        	createEventPop.hide();
        }
        $('#CreateEvent').show();
    }
    if (!searchPanel.is(e.target) && searchPanel.has(e.target).length === 0)
    {
        searchPanel.hide();
        $('#searchConversation').show();
    }
    if (!filterPannel.is(e.target) && filterPannel.has(e.target).length === 0)
    {
        filterPannel.hide();
        $('.side-bar-filter-icon').removeClass('active');
        $('.chooseTag').hide();
    }
    if (!moreMenuPopup.is(e.target) && moreMenuPopup.has(e.target).length === 0)
    {
        moreMenuPopup.hide();
    }
    if (!moreMenumsgPopup.is(e.target) && moreMenumsgPopup.has(e.target).length === 0)
    {
        moreMenumsgPopup.hide();
    }
    if (!repEmojiDiv.is(e.target) && repEmojiDiv.has(e.target).length === 0)
    {
        repEmojiDiv.remove();
    }
});

// create to do popup

var sideBarSearchcollapses =()=>{
	$(".side-bar-search-icon").mouseenter(function() {
	  $(this).hide();
	  if($(".thread_active").is(":visible") == true){
	  	$(".thread_active").hide();
	  	$(".side_bar_thread_ico").show();
	  }
	  if($('#sideBarSearch').is(':visible') == false){
	  	$('#sideBarSearch').show();
	  }
	  });


	$('#sideBarSearch').mouseleave(function() {

		if($('#sideBarSearch').is(':focus') == false && $('#sideBarSearch').val().length < 1){
			$(this).hide();
			$(".side-bar-search-icon").show();
		}

	});

	$('#sideBarSearch').blur(function(){
	    if($('#sideBarSearch').val().length < 1){
			$(this).hide();
			$(".side-bar-search-icon").show();
		}

	});
}
sideBarSearchcollapses();


var viewThread =()=>{
	$(".msgs-form-users").on('click', function(){
		$(".side_bar_thread_ico").toggle();
		$(".thread_active").toggle();
	});
}
viewThread();
$(".thread_active").on('click', function(){
	$('#connectAsideContainer').hide();
	$(".threadasideContainer").show();
})
var threadReply = (event) =>{
	if($('#threadReplyPopUp').is(":visible") == false){
		var msgid = $(event.target).closest('.msgs-form-users').attr('data-msgid');
	    var count_no_msg = 0;

	    $.ajax({
	      url: "/hayven/open_thread",
	      type: "POST",
	      data: { msg_id: msgid, conversation_id: conversation_id },
	      dataType: "JSON",
	      success: function(threadrep) {
	        thread_id = threadrep;
	        thread_root_id = msgid;

			/* main thread msg html design */
			var main_msg_body = '<div class="thread-user-photo">'+$('.msg_id_'+msgid).find('.msg-user-photo').html()+'</div>';
			main_msg_body += '<div class="thread-user-msg"><h4>';
			main_msg_body += $('.msg_id_'+msgid).find('.user-msg h4').html();
			main_msg_body += '</h4>';
			main_msg_body += '<p>';
			main_msg_body += $('.msg_id_'+msgid).find('.user-msg p').html();
			main_msg_body += '</p>';
			main_msg_body += '</div>';
			$('#threadReplyPopUp .main-thread-msgs').html(main_msg_body);
			$('#threadReplyPopUp .main-thread-msgs').find('.msg-time').addClass('thread-msg-time').removeClass('msg-time');
			/* end of main thread msg html design */

			$('#threadReplyPopUp .replies-container').html("");
			$('#threadReplyPopUp').show();
			$('.write-thread-msgs input').focus();

			find_and_show_reply_msg(msgid);
	      },
	      error: function(err) {
	        console.log(err.responseText);
	      }
	    });
	}
};
var find_and_show_reply_msg = (msgid) => {
  var noofreply = parseInt($('.msg_id_'+msgid).find('.no-of-replies').text());
  $('.reply-separetor p').html(noofreply+' Reply');
  if (noofreply > 0) {
    socket.emit('find_reply', { msg_id: msgid, conversation_id: conversation_id }, (reply_list) => {
      if (reply_list.status) {
        var reply_list_data = _.sortBy(reply_list.data, ["created_at", ]);

        // var need_update_reply_message_seen_list = [];

		$.each(reply_list_data, function(key, row) {
			// if (row.msg_status == null) {
			// 	if (row.sender == user_id) {
			// 		// This msg send by this user; so no need to change any seen status
			// 	} else {
			// 		// This msg receive by this user; so need to change seen status
			// 		need_update_reply_message_seen_list.push(row.msg_id);
			// 	}
			// }
			draw_rep_msg(row);
		});


        // if (need_update_reply_message_seen_list.length > 0) {
        //   $.ajax({
        //     url: '/hayven/update_msg_status',
        //     type: 'POST',
        //     data: {
        //       msgid_lists: JSON.stringify(need_update_reply_message_seen_list),
        //       user_id: user_id
        //     },
        //     dataType: 'JSON',
        //     success: function(res) {
        //       socket.emit('update_msg_seen', {
        //         msgid: need_update_reply_message_seen_list,
        //         senderid: to,
        //         receiverid: user_id,
        //         conversation_id: conversation_id
        //       });
        //     },
        //     error: function(err) {
        //       console.log(err);
        //     }
        //   });
        // }

        // separetor_show_hide();
      } else {
        console.log('replay search query error', reply_list); // error meessage here
      }
    });
  }
};
var draw_rep_msg = (row) =>{
	var html = 	'<div class="main-thread-msgs" style="margin-top:18px;">';
		html += 	'<div class="thread-user-photo">';
		html +=			'<img src="/images/users/'+ row.sender_img +'" alt="">';
		html += 	'</div>';
		html +=		'<div class="thread-user-msg">';
		html += 		'<h4>'+ row.sender_name + '&nbsp;<span class="thread-msg-time">'+ moment(row.created_at).format('h:mm a') +'</span></h4>';
		html += 		'<p>'+ row.msg_body + '</p>';
		html += 	'</div>';
		html += '</div>';
	$('#threadReplyPopUp .replies-container').append(html);
}
var draw_rep_count = (msgid) =>{
	var noofreply = Number($('.msg_id_'+msgid).find('.no-of-replies').text());
    if(noofreply>0){
        $('.msg_id_'+msgid).find('.no-of-replies').text(noofreply+1);
    }else{
        var html = per_msg_rep_btn(1);
        $('.msg_id_'+msgid).find('.user-msg').append(html);
    }
    $('.reply-separetor p').html( (noofreply+1) +' Reply');
};
function closeAllPopUp(){
	if($('#threadReplyPopUp').is(":visible")){
		$('#threadReplyPopUp').hide();
	};
	if($('#createToDoPopup').is(":visible")){
		$('#createToDoPopup').hide();
	};
	if($('#threadReplyPopUpSlider').is(":visible")){
		$('#threadReplyPopUpSlider').hide();
	};
}

function backToChat(){
	$(".threadasideContainer").hide();
	$('#connectAsideContainer').show();
	$(".side_bar_list_item li").show();
	$('#connectAsideContainer .backToChat').hide();
}
function viewCreateTodoPopup(){
	$('#createToDoPopup').show();
}
function viewtodoAdOp(){
	$('#toggle_area').toggle();
}
function viewAllThread(){
	$('#threadReplyPopUpSlider').show();
}
var threadSlider =()=>{

	$('.nextThread').on('click', function(){
		$(this).parent('.ThreadPopUp').hide();
		$('.ThreadPopUp').next('.ThreadPopUp').show();
	});
	$('.pevThread').on('click', function(){
		$(this).parent('.ThreadPopUp').hide();
		$('.ThreadPopUp').prev('.ThreadPopUp').show();
	});
}
threadSlider();

/* Flag and unflag */
var flggUserMsg = (event) =>{
	var flaggedMsg ='&nbsp;<img class="flaggedMsg" src="/images/basicAssets/Flagged.svg">';
	var flagged ='<img src="/images/basicAssets/Flagged.svg" alt="Flagged">';
	var not_flagged = '<img src="/images/basicAssets/NotFlagged.svg" alt="Not Flagged">';

	var msgid = $(event.target).closest('.msgs-form-users').attr('data-msgid');

	if($(event.target).closest('.msgs-form-users').find(".flaggedMsg").length == 1){
		$.ajax({
			url: '/hayven/flag_unflag',
			type: 'POST',
			data: { uid: user_id, msgid: msgid, is_add: 'no' },
			dataType: 'JSON',
			success: function(res) {
				if (res.status) {
					$(event.target).closest(".msgs-form-users").find(".flaggedMsg").remove();
					$(event.target).closest(".msgs-form-users").css('background-color', 'transparent');
					$(event.target).closest(".msgs-form-users").find(".flag").html(not_flagged);
				}
			},
			error: function(err) {
				console.log(err.responseText);
			}
		});
	}else{
		$.ajax({
			url: '/hayven/flag_unflag',
			type: 'POST',
			data: { uid: user_id, msgid: msgid, is_add: 'yes' },
			dataType: 'JSON',
			success: function(res) {
				if (res.status) {
					$(event.target).closest(".msgs-form-users").find(".user-msg h4").append(flaggedMsg);
					$(event.target).closest(".msgs-form-users").css('background-color', 'rgba(224, 60, 49, 0.04)');
					$(event.target).closest(".msgs-form-users").find(".flag").html(flagged);
				}
			},
			error: function(err) {
				console.log(err.responseText);
			}
		});
	}
};
/* End flag */

/* Start Emoji */
var viewEmojiList = (event) => {
	var design 	= '<div class="emojiListContainer">';
	    design += 	'<img src="/images/emoji/grinning.png" data-name="grinning" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/joy.png" data-name="joy" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/open_mouth.png" data-name="open_mouth" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/disappointed_relieved.png" data-name="disappointed_relieved" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/rage.png" data-name="rage" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/thumbsup.png" data-name="thumbsup" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/thumbsdown.png" data-name="thumbsdown" onclick="add_reac_into_replies(event)">';
		design += 	'<img src="/images/emoji/heart.png" data-name="heart" onclick="add_reac_into_replies(event)">';
		design += '</div>';


	if(!$(event.target).closest(".call-rep-emoji").find(".emojiListContainer").length == 1){
		$(event.target).closest(".call-rep-emoji").append(design);
	}
	// else{
	// 	$(event.target).closest(".emoji").find(".emojiListContainer").remove();
	// }
};
var add_reac_into_replies = (event) => {
  var msg_id = $(event.target).closest('.msgs-form-users').attr('data-msgid');
  var src = $(event.target).attr('src');
  var emojiname = $(event.target).attr('data-name');

  $.ajax({
    url: '/hayven/add_reac_emoji',
    type: 'POST',
    data: { msgid: msg_id, emoji: emojiname },
    dataType: 'JSON',
    success: function(res) {
		if (res.status) {
			if (res.rep == 'add') {
				append_reac_emoji(msg_id, src, 1);
				socket.emit("emoji_emit", { room_id: to, msgid: msg_id, emoji_name: emojiname, count: 1, sender_id: user_id });
			} else if (res.rep == 'delete') {
				update_reac_emoji(msg_id, src, -1);
			} else if (res.rep == 'update') {
				update_reac_emoji(msg_id, '/images/emoji/' + res.old_rep + '.png', -1);
				append_reac_emoji(msg_id, src, 1);
			}
		}
    },
    error: function(err) {
		console.log(err.responseText);
    }
  });
};
var append_reac_emoji = (msgid, src, count) => {
	var allemoji = $('.msg_id_' + msgid).find('.emoji img');
	if (allemoji == undefined) {
		emoji_html_append(msgid, src, count);
	} else {
		var noe = 0;
		$.each(allemoji, function(k, v) {
			if ($(v).attr('src') == src) {
				noe = parseInt($(v).next('.count-emoji').text());
				$(v).next('.count-emoji').text(noe + 1);
			}
		});
		if (noe === 0) {
			emoji_html_append(msgid, src, count);
		}
	}
	$('.emojiListContainer').remove();
};
var update_reac_emoji = (msgid, src, count) => {
	var allemoji = $('.msg_id_' + msgid).find('.emoji img');

	var noe = 0;
	$.each(allemoji, function(k, v) {
		if ($(v).attr('src') == src) {
			noe = parseInt($(v).next('.count-emoji').text());
			if (noe == 1)
				$(v).closest('.emoji').remove();
			else
				$(v).next('.count-emoji').text(noe - 1);
		}
	});

	$('.emojiListContainer').remove();
};
var emoji_html_append = (msgid, src, count) => {
	var emoji_name = ((src.split('/'))[3]).replace('.png', '');
	var html = emoji_html(emoji_name, src, count);
	$('.msg_id_' + msgid).find('.replies').append(html);
};
var emoji_html = (emoji_name, src, count) =>{
	var html = '<span class="emoji '+emoji_name+' " onmouseover="open_rep_user_emo(event)" onmouseout="close_rep_user_emo(event)">';
	html += '<img src="' + src + '"> ';
	html += '<span class="count-emoji">' + count + '</span>';
	html += '</span>';
	return html;
}
var open_rep_user_emo = (event) => {
	console.log("open_rep_user_emo");
	if ($('.rep_user_emo_list').length == 0) {
		var msg_id = $(event.target).closest('.msgs-form-users').attr('data-msgid');
		var emoji_name = (($(event.target).closest('.emoji').find('img').attr('src').split('/'))[3]).replace('.png', '');
		$.ajax({
			url: '/hayven/emoji_rep_list',
			type: 'POST',
			data: { msgid: msg_id, emojiname: emoji_name },
			dataType: 'JSON',
			success: function(res) {
				if (res.length > 0) {
					var html = '<div class="rep_user_emo_list">';
					$.each(res, function(k, v) {
						html += v.user_fullname + '<br>';
					});
					html += '</div>';
					$('.msg_id_' + msg_id).find('.'+ emoji_name).append(html);
					var div_offset = $(event.target).closest('.emoji').offset();
					// console.log(div_offset);
					$('.rep_user_emo_list').css('left', div_offset.left - 300);
				}
			},
			error: function(err) {
				console.log(err.responseText);
			}
		});
	}
};
var close_rep_user_emo = (event) => {
	$('.rep_user_emo_list').remove();
};
/* End Emoji */

/* Start messages searching */
$("#search-msg").on('keyup', function(event){
	var str = $('#search-msg').val();
	str = str.replace(/<\/?[^>]+(>|$)/g, "");
	// console.log(str);
	$('.user-msg>p').unhighlight();
	$('.user-msg>p').highlight(str);
	if ($('.highlight').length > 0) {
		$.each($('.msgs-form-users'), function() {
			if ($(this).find('.highlight').length == 0) {
				$(this).prev('.msg-separetor').hide();
				$(this).hide();
			} else {
				$(this).prev('.msg-separetor').show();
				$(this).show();
			}
		});
	} else {
		$('.msg-separetor').show();
		$('.msgs-form-users').show();
	}
});
/* End messages searching */

/* Filtaring  */
var show_flag_msg = () =>{
	$.each($('.msgs-form-users'), function() {
		if ($(this).find('.flaggedMsg').length == 0) {
			$(this).prev('.msg-separetor').hide();
			$(this).hide();
		} else {
			$(this).prev('.msg-separetor').show();
			$(this).show();
		}
	});
};

var filter_unread = () =>{
	$('#connectAsideContainer .backToChat').show();
	var click_1st_unread_thread = false;
	$.each($(".side_bar_list_item li"), function(k, per_li){
		var has_unread = Number($(per_li).find(".unreadMsgCount").html());
		if(has_unread<1){
			$(per_li).hide();
		}
	});
	$.each($(".side_bar_list_item li:visible"), function(k, per_li){
		var has_unread = Number($(per_li).find(".unreadMsgCount").html());
		if(has_unread>0 && click_1st_unread_thread === false){
			$(per_li).trigger("click");
			click_1st_unread_thread = true;
			return false;
		}
	});
};
/* End Filtaring */

// media popup
$('.media-tabs>li').click(function () {
	$('.media-tabs>li').removeClass("active");
	$(this).addClass("active");
});

function viewImgDiv() {
	$('.media_Tab_Content').hide();
	$("#mediaImages").show();
}

function viewvideoDiv() {
	$('.media_Tab_Content').hide();
	$("#mediaVideos").show();
}
function viewFilesoDiv() {
	$('.media_Tab_Content').hide();
	$("#mediaFiles").show();
}

function viewLinksDiv() {
	$('.media_Tab_Content').hide();
	$("#mediaLinks").show();
}
var mediaFilePopup = () => {
	$('#mediaFileBackWrap').show();

	$("#mediaImages").html("");
	var allimg = $('.img_attach');
	// console.log(allimg);

	$.each(allimg, function(k,v){
		var unixt = Number((v.alt).substring((v.alt).lastIndexOf('@')+1, (v.alt).lastIndexOf('.')));
		// console.log(moment(Number(unixt)).format());
		var msg_date = moment(unixt).calendar(null, {
	        sameDay: '[Today]',
	        lastDay: '[Yesterday]',
	        lastWeek: function(now) {return '['+this.format("MMM Do, YYYY")+']';},
	        sameElse: function(now) {return '['+this.format("MMM Do, YYYY")+']';}
		});

		$.each($('.date-by-images h3'), function(dk, dv) {
			if ($(dv).text() == msg_date) {
				msg_date = null;
				return 0;
			}
		});

		if(msg_date !== null){
			var dataofimg = '<div class="date-by-images"><h3>'+ msg_date +'</h3></div>';
			$("#mediaImages").prepend(dataofimg);
		}

		var html = '';
		html += 	'<div class="all-images" onclick="showImageSlider(event)" data-src="'+ v.src +'" data-filename="'+ v.alt +'" data-sender_name="'+ $(v).attr('data-sender_name') +'" data-sender_img="'+ $(v).attr('data-sender_img') +'">';
		html +=			'<img src="'+ v.src +'" alt="'+ v.alt +'" style="pointer-events: none;">';
		html += 	'</div>';
		$("#mediaImages .date-by-images").append(html);
	});
}
var showImageSlider = (event) => {
	$('.media-file-popup').hide();
	$('.image-popup-slider').show();

	var curimg = $(event.target).attr('data-src');
	var imgsn = $(event.target).attr('data-sender_name');
	var img = $(event.target).attr('data-sender_img');

	var allthisdateimg = $(event.target).closest('.date-by-images').find('.all-images');
	$('.images-slider-footer').html("");
	$.each(allthisdateimg, function(k,v){
		var src = $(v).attr('data-src');
		var name = $(v).attr('data-sender_name');
		var img = $(v).attr('data-sender_img');
		var html = 	'<div class="slider-footer-all-images" onclick="activethisimg(\''+src+'\', \''+name+'\', \''+img+'\')">';
			html += 	'<img src="'+ $(v).attr('data-src') +'" alt="">';
			html += '</div>';
		$('.images-slider-footer').append(html);
	});
	activethisimg(curimg, imgsn, img);
};
var activethisimg = (curimg, imgsn, img) =>{
	$('.image-popup-slider').find('.currentimg').attr('src', curimg);
	$('.image-popup-slider').find('.shared-by-user-photo>img').attr('src', '/images/users/'+img);
	$('.image-popup-slider').find('.shared-by-user-details>h3').html(imgsn);
	$('.slider-footer-all-images img').removeClass('active');
	$.each($('.slider-footer-all-images img'), function(k,v){
		if($(v).attr('src') == curimg)
			$(v).addClass('active');
	});
};
// slider left arrow a click = sla
var slaclick = () => {
	var pref_el = $('img.active').closest('.slider-footer-all-images').prev();
	console.log(pref_el.length);
	if(pref_el.length){
		pref_el.trigger('click');
	}
	else{
		$('.slider-footer-all-images').last().trigger('click');
	}
};
// slider right arrow a click = sra
var sraclick = () => {
	var next_el = $('img.active').closest('.slider-footer-all-images').next();
	if(next_el.length)
		next_el.trigger('click');
	else
		$('.slider-footer-all-images').first().trigger('click');
};
var backToMediaTab = () => {
	$('.image-popup-slider').hide();
	$('.media-file-popup').show();
};
var backToMediaTab = () => {
	$('.image-popup-slider').hide();
	$('.media-file-popup').show();
};
function mediaFilePopup() {
	$('#mediaFileBackWrap').show();
}

function closeMediaPopup() {
	$('#mediaFileBackWrap').hide();
}



function acceptToDo(event){
	var html   = '<div class="toDoContent_Sec1">';
		html  += 	'<div class="acceptCheck"></div>';
		html  += 	'<p class="acceptedLabel">Youve accepted <label>Amazon Wishlist.</label></p>';
		html  += '</div>';
		html  += '<div class="toDoContent_Sec2">';
		html  += 	'<h1 class="acceptedMember">Accepted Member</h1>';
		html  += 	'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'<span class="countSelected">+4</span>';
		html  += '</div>';

	$(event.target).closest('.toDoContent').html(html);
}

function viewEventMember(event){
	var html   = '<div class="event_content_sec1">';
		html  += 	'<div class="acceptCheck"></div>';
		html  += 	'<p class="acceptedLabel">Youve accepted <label>Amazon Wishlist.</label></p>';
		html  += '</div>';
		html  += '<div class="event_content_sec2">';
		html  += 	'<h1 class="attendingMemberLabel">Attending</h1>';
		html  += 	'<h1 class="maybeAttMemberLabel">Maybe Attending</h1>';
		html  += 	'<h1 class="notAttMemberLabel">Not Attending</h1>';
		html  += 	'<div class="attendingMemberCount">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'</div>';
		html  += 	'<div class="maybeAttMemberCount">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<span class="countSelected">+4</span>';
		html  += 	'</div>';
		html  += 	'<div class="notAttMemberCount">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 		'<img src="/images/users/joni.jpg" class="selected_member">';
		html  += 	'</div>';
		html  += '</div>';

	$(event.target).closest('.event_content').html(html);
}




var calendarPicker = ()=>{
	$("#eventDateAndTime").on('click', function(){
		$("#calenderPicker").show();
	});

	$("#calenderDoneBtn").on('click', function(){
		$("#calenderPicker").hide();
	})
	$("#calenderCancelBtn").on('click', function(){
		$("#calenderPicker").hide();
	})

}
calendarPicker();


var escKeyUpForConnect = ()=>{
	$(window).keyup(function(e){
		if (e.keyCode == 27) {
			// filter sidebar
			if($('.filterMainContainer').is(":visible") == true && $('.chooseTag').is(":visible") == true){
				$('.chooseTag').hide();

			}else
			if($('.filterMainContainer').is(":visible") == true){
				$('.filterMainContainer').hide();
				$('.side-bar-filter-icon').removeClass("active");
			}
			// create to do chat header
			if($('.create-todo-popup').is(":visible") == true && $("#todoAdvanceOption").is(":visible") == true){
				$("#todoAdvanceOption").hide();

			}else
			if($('.create-todo-popup').is(":visible") == true){
				$('.create-todo-popup').hide();
				$("#createTodo").show();
			}
			// create event chat header
			if($('.create-event-popup').is(":visible") == true && $("#eventAdvanceOption").is(":visible") == true){
				$("#eventAdvanceOption").hide();

			}else
			if($('.create-event-popup').is(":visible") == true){
				$('.create-event-popup').hide();
				$("#CreateEvent").show();
			}
			// search bar chat header
			if($('.search-panel').is(":visible") == true){
				$('.search-panel').hide();
				$("#searchConversation").show();
			}
			// more option chat header
			if($('.moreMenuPopup').is(":visible") == true && $('.backwrap').is(":visible") == true){
				$('.backwrap').hide();
			}else
			if($('.moreMenuPopup').is(":visible") == true){
				$('.moreMenuPopup').hide();
			}
			// msg hover
			if($('.msgs-form-users-options').is(":visible") == true && $('.emojiListContainer').is(":visible") == true){
				$('.emojiListContainer').hide();
			}else
			if($('.msgs-form-users-options').is(":visible") == true && $('.msg-more-popup').is(":visible") == true){
				$('.msg-more-popup').hide();
			}else
			if($("#threadReplyPopUp").is(":visible") == true){
				$("#threadReplyPopUp").hide();
			}

			//thread sidebar container
			if($('.threadasideContainer').is(':visible') == true){
				$('.threadasideContainer').hide();
				$("#connectAsideContainer").show();
			}
			//// create Tagged Container
			if($('.chat-head-calling .addTagConv').is(':visible') == true){
				$('.chat-head-calling .addTagConv').hide();
				$('.chat-head-calling .tagged').show();
				$(this).val("");
			}
			//// create Tagged Container
			if($('#createDirMsgContainer').is(":visible") == true && $('.add-direct-member').is(':focus') == false || $('#createChannelContainer').is(':visible') == true && $('.add-team-member').is(':focus') == false && $('#team-name').is(':focus') == false || $('#joinChannelPanel').is(':visible') == true ){
				$('.connect_right_section').hide();
				$('#groupChatContainer').show();
			}

  		}
	});
}

escKeyUpForConnect();


//create new event
//create new event
function createEventF(event){
	event.preventDefault();
	var eventName = $('.create-event-popup-title').val();
	var eventLocation = $('.event-location').val();
	var design  = '<div class="msgs-form-users">';
		design += 	'<div class="msg-user-photo">';
		design += 		'<img src="/images/users/nayeem.jpg" alt="">';
		design += 	'</div>';
		design += 	'<div class="user-msg">';
		design += 		'<h4>George Jsons<span class="msg-time">3:12 PM</span></h4>';
		design += 		'<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>';
		design += 		'<div class="event_content">';
		design += 			'<div class="event_content_sec1">';
		design += 				'<img src="/images/basicAssets/custom_event_for_msg.svg">';
		design += 				'<p class="toDoName">'+eventName+'</p>';
		design += 				'<span class="dateAndTime">When: 14 September, 2018(3:30pm - 4:30pm)</span>';
		design += 				'<span class="location">Where: '+eventLocation+'</span>';
		design += 			'</div>';
		design += 			'<div class="event_content_sec2">';
		design += 				'<button class="attending" onclick="viewEventMember(event)">Attending</button>';
		design += 				'<button class="maybe_attending" onclick="viewEventMember(event)">Maybe Attending</button>';
		design += 				'<button class="not_attending" onclick="viewEventMember(event)">Not Attending</button>';
		design += 			'</div>';
		design += 		'</div>';
		design += 	'</div>';
		design += 	'<div class="msgs-form-users-options" style="top: 16px;">';
		design += 		'<div class="call-rep-emoji" onclick="viewEmojiList(event)">';
		design += 			'<img src="/images/basicAssets/AddEmoji.svg" alt="">';
		design += 		'</div>';
		design += 		'<div class="flag" onclick="flggUserMsg(event)">';
		design += 			'<img src="/images/basicAssets/NotFlagged.svg" alt="">';
		design += 		'</div>';
		design += 		'<div class="replys" onclick="threadReply()">';
		design += 			'<img src="/images/basicAssets/Thread.svg" alt="">';
		design += 		'</div>';
		design += 		'<div class="more">';
		design += 			'<img src="/images/basicAssets/MoreMenu.svg" alt="">';
		design += 			'<div class="msg-more-popup" style="display:none">';
		design += 				'<p onclick="viewCreateTodoPopup()">Create a to do</p>';
		design += 				'<p>Schedule an event</p>';
		design += 				'<p>Start a poll</p>';
		design += 				'<p>Share Message</p>';
		design += 			'</div>';
		design += 		'</div>';
		design += 	'</div>';
		design += '</div>';
	if(eventName != "" && eventLocation != ""){
		$('#msg-container').append(design);
		$('.create-event-popup-title').val("");
		$('.event-location').val("");
		$('.create-event-popup').hide();
		$('#CreateEvent').show();
		moreMsgAction();
	}
}




/////////// create new tag for conversation

var addNewTagConv = ()=>{

	$('.chat-head-calling .tagged').on('click', function(){
		$(this).hide();
		$('.chat-head-calling .addTagConv').show();
		$("#createConvTag").focus();
	});
}
addNewTagConv();



// create new to do


var addNewTodo = ()=>{
	$('.create-todo-popup-title').on('keyup', function(e){
		var toDoName = $(this).val();
		if(toDoName != ""){
		var design  = '<div class="msgs-form-users">';
			design += 	'<div class="msg-user-photo">';
			design += 		'<img src="/images/users/joni.jpg" alt="">';
			design += 	'</div>';
			design += 	'<div class="user-msg">';
			design += 		'<h4>Dalim Chy<span class="msg-time">3:12 PM</span></h4>';
			design += 		'<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>';
			design +=		'<div class="toDoContent">';
			design +=			'<div class="toDoContent_Sec1">';
			design +=				'<img src="/images/basicAssets/custom_to_do_for_msg.svg">';
			design +=				'<p class="toDoName">'+toDoName+'</p>';
			design +=				'<p>Due Date: 14 September, 2018</p>';
			design +=			'</div>';
			design +=			'<div class="toDoContent_Sec2">';
			design +=				'<button class="accept_toDO" onclick="acceptToDo(event)">Accept</button>';
			design +=				'<button class="decline_toDo">Decline</button>';
			design +=			'</div>';
			design +=		'</div>';
			design += 	'</div>';
			design += 	'<div class="msgs-form-users-options" style="top: 16px;">';
			design += 		'<div class="call-rep-emoji" onclick="viewEmojiList(event)">';
			design += 			'<img src="/images/basicAssets/AddEmoji.svg" alt="">';
			design += 		'</div>';
			design += 		'<div class="flag" onclick="flggUserMsg(event)">';
			design += 			'<img src="/images/basicAssets/NotFlagged.svg" alt="">';
			design += 		'</div>';
			design += 		'<div class="replys" onclick="threadReply(event)">';
			design += 			'<img src="/images/basicAssets/Thread.svg" alt="">';
			design += 		'</div>';
			design += 		'<div class="more">';
			design += 			'<img src="/images/basicAssets/MoreMenu.svg" alt="">';
			design += 			'<div class="msg-more-popup" style="display:none">';
			design += 				'<p onclick="viewCreateTodoPopup()">Create a to do</p>';
			design += 				'<p>Schedule an event</p>';
			design += 				'<p>Start a poll</p>';
			design += 				'<p>Share Message</p>';
			design += 			'</div>';
			design += 		'</div>';
			design += 	'</div>';
			design += '</div>';

			if(e.keyCode == 13){
				$('#msg-container').append(design);
				$(this).val("");
				$(".create-todo-popup").hide();
				$("#createTodo").show();
			}
			moreMsgAction();
		}

	});

}
addNewTodo();
