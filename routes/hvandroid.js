var express = require('express');
var bcrypt = require('bcryptjs');
var FCM = require('fcm-node');
var multer = require('multer');
var serverKey = 'AAAAwSuL-Gg:APA91bGQeZg_iF_nu7zWvGq4XfkPKRas5H8T8BVKL3Ve8o5HqKHQh2vMcWZYL4W5kl1fUPqd8osSP4EXNA59Y2QstNSd1S0MoptoXRCusSia1-ql62fapg8NT7tRlAuxBHRnEqeNxE4c'; //put your server key here
var fcm = new FCM(serverKey);
var _ = require('lodash');
var moment = require('moment');
var router = express.Router();
var inArray = require('in-array');
var {models} = require('./../config/db/express-cassandra');
var {passwordToHass, passwordToCompare} = require('./../utils/hassing');
var {getActiveUsers} = require('./../utils/chatuser');
var {saveConversation, findConversationHistory,checkAdmin,createPersonalConv,check_only_Creator_or_admin} = require('./../utils/conversation');
var {
  generateMessage, 
  sendNewMsg, 
  sendBusyMsg, 
  commit_msg_delete, 
  flag_unflag, 
  add_reac_emoji, 
  view_reac_emoji_list, 
  get_group_lists, 
  update_msg_status_add_viewer, 
  check_reac_emoji_list, 
  delete_reac_emoji, 
  update_reac_emoji,
  get_messages_tag,
  getAllUnread,
  getPersonalConversation,
  replyId
} = require('./../utils/message');


// creates a configured middleware instance
// destination: handles destination
// filenane: allows you to set the name of the recorded file
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(`./public/upload/`))
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname.replace(path.extname(file.originalname), '@') +Date.now() +  path.extname(file.originalname));
    }
});

// utiliza a storage para configurar a instância do multer
const upload = multer({ storage });


/* GET login page. */
router.get('/', function(req, res, next) {
  if(req.session.login){
    res.redirect('/hayven');
  } else {
    res.send('This is android login page gitlab project');
  }
});

/* POST login listing. */
router.post('/login', function(req, res, next) {
  models.instance.Users.find({}, {raw:true, allow_filtering: true}, function(err, users){
      if(err) throw err;
      //user is an array of plain objects with only name and age
      var alluserlist = [];
      var user = [];
      var msg = '';
      _.each(users, function(v,k){
        alluserlist.push({id: v.id, dept: v.dept, designation: v.designation, email: v.email, fullname: v.fullname, img: v.img});
        
        // Removing the old gcm_id if any
        var query_object = {id: v.id};
        var update_values_object = {gcm_id: null};
        var options = {conditions: {gcm_id: req.body.gcm_id}}; 
        if(req.body.gcm_id == v.gcm_id && req.body.email != v.email){
          models.instance.Users.update(query_object, update_values_object, options, function(err){
              if(err) console.log(err);
              else console.log('Removing the old gcm_id if any');
          }); 
        }
        // End of removing the old gcm_id if any

        if(req.body.email == v.email){
          if(passwordToCompare(req.body.password, v.password)){
            msg = "true";
            user = {id: v.id, dept: v.dept, designation: v.designation, email: v.email, fullname: v.fullname, img: v.img};
            
            query_object = {id: v.id};
            update_values_object = {gcm_id: req.body.gcm_id};
            options = {conditions: {email: req.body.email}};
            models.instance.Users.update(query_object, update_values_object, options, function(err){
                if(err) console.log(err);
                else console.log('Successfully update the gcm id');
            });
          }else{
            msg = 'Password does not match';
          }
        }
      });
      if(msg == ""){
        res.send({alluserlist: [], user: {}, msg: "Email address is invalid"});
      } 
      else if(msg == "Password does not match"){
        res.send({alluserlist: [], user: {}, msg: "Password does not match"});
      }
      else{
        
        var query = {
          participants: { $contains: user.id.toString() },
          group: { $eq: 'yes' },
          single: { $eq: 'no' }
        };
    
        models.instance.Conversation.find( query, { raw: true, allow_filtering: true }, function(err, conversations) {
          if (err) throw err;
          
          getActiveUsers((uresult, uerror) => {
            if (uerror) console.log(uerror);

            var blockUserListArray = [];
            var blockGroupListArray = [];

            if((uresult.users).length>0){
              _.each(uresult.users, function(users,k){
                if(users.id.toString() !== user.id.toString() ){
                  blockUserListArray.push({
                    userid: users.id,
                    conversation_id:"",
                    conversation_type: "personal",
                    users_name: users.fullname,
                    users_img: users.img
                  });
                }
              });
            }

            if((conversations).length>0){
              _.each(conversations, function(group,k){
                blockGroupListArray.push({
                  userid: user.id.toString(),
                  conversation_id: group.conversation_id,
                  conversation_type: "group",
                  users_name: group.title,
                  users_img: 'feelix.jpg'
                });
              });
            }

            var finalListArray = blockUserListArray.concat(blockGroupListArray);
            
            getPersonalConversation(user.id.toString(), (cnvRes,cnvErr)=>{
              if(cnvErr){
                throw cnvErr;
              }else{
                
                var unreadUnpinArray = [];

                _.each(cnvRes.conversations, function(value,key){
                  _.each(value.participants, function(v,k){
                    if(v != user.id.toString()){
                      _.each(finalListArray, function(vf,kf){
                        if(vf.userid == v){
                          vf.conversation_id = value.conversation_id.toString();
                          unreadUnpinArray.push(value.conversation_id.toString());
                        }
                      });
                    }
                  });
                });

                getAllUnread(user.id.toString(), (urRes, urErr)=>{
                  if(urErr){
                    throw urErr;
                  }else{
      
                    var splitunpinnd =[];
                    var mdataunpinned =[];
                    var readMsgArr =[];
                    var readMsgArrCount =[];
      
                    var msgbodyArray = _.orderBy(urRes.unreadmsgbody, ['created_at'], ['desc']);
                    var ReadmsgbodyArray = _.orderBy(urRes.Readmsgbody, ['created_at'], ['desc']);
                    
                    _.each(msgbodyArray, function(value){
                      if(mdataunpinned.indexOf(value.conversation_id.toString()) == -1){
                        splitunpinnd.push(value);
                        mdataunpinned.push(value.conversation_id.toString());
                      }else{
                        mdataunpinned.push(value.conversation_id.toString());
                      }
                    });

                    _.each(ReadmsgbodyArray, function(value){
                      if(mdataunpinned.indexOf(value.conversation_id.toString()) == -1){
                        readMsgArr.push(value);
                        mdataunpinned.push(value.conversation_id.toString());
                      }
                    });

                    
                    var counts = {};
                    var newBody = [];
                    var myUserList = [];
                    var newBodyRead = [];
                    var myConversationList = urRes.unique;
                    
                    mdataunpinned.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });

                    _.each(finalListArray, function(value){
                      if(myConversationList.indexOf(value.conversation_id.toString()) !== -1){
                        myUserList.push(value);
                      }
                    });

                    _.each(readMsgArr, function(v,k){
                      newBodyRead.push({
                        'conversation_id':v.conversation_id,
                        'msg_body':v.msg_body,
                        'created_at':v.created_at,
                        'msg_id':v.msg_id,
                        'msg_type':v.msg_type,
                        'sender_img':v.sender_img,
                        'sender_name':v.sender_name,
                        'totalUnread':0
                      });
                    });

                    _.each(splitunpinnd, function(v,k){
                      newBody.push({
                        'conversation_id':v.conversation_id,
                        'msg_body':v.msg_body,
                        'created_at':v.created_at,
                        'msg_id':v.msg_id,
                        'msg_type':v.msg_type,
                        'sender_img':v.sender_img,
                        'sender_name':v.sender_name,
                        'totalUnread':counts[v.conversation_id.toString()]
                      });
                    });

                    var newBodyReadUnread = newBodyRead.concat(newBody);
                    var androidMsgList = [];
                    var totalmesgList = [];

                    _.each(myUserList, function(vm,k){
                      _.each(newBodyReadUnread, function(vs,k){
                        if(vm.conversation_id == vs.conversation_id){
                          androidMsgList.push(vs.conversation_id);
                          totalmesgList.push({
                            'conversation_id':vs.conversation_id,
                            'conversation_type':vm.conversation_type,
                            'conversation_title':vm.users_name,
                            'msg_body':vs.msg_body,
                            'created_at':vs.created_at,
                            'msg_id':vs.msg_id,
                            'msg_type':vs.msg_type,
                            'sender_img':vs.sender_img,
                            'sender_name':vs.sender_name,
                            'totalUnread': vs.totalUnread
                          });
                        }
                      });
                    });

                    _.each(myUserList, function(vm,k){
                      if(androidMsgList.indexOf(vm.conversation_id.toString()) == -1){
                        totalmesgList.push({
                          'conversation_id':vm.conversation_id,
                          'conversation_type':vm.conversation_type,
                          'conversation_title':vm.users_name,
                          'msg_body':"",
                          'created_at':moment().format('YYYY-MM-DDTHH:mm:ssZ'),
                          'msg_id':"",
                          'msg_type':"",
                          'sender_img':vm.users_img,
                          'sender_name':vm.users_name,
                          'totalUnread': 0
                        });
                      }
                    });

                    var RallMesgRaaay = _.orderBy(totalmesgList, ['created_at'], ['desc']);
                    
                    res.send({
                      alluserlist: alluserlist,
                      conversations:conversations, 
                      user: user, 
                      msg: msg, 
                      myUserList:RallMesgRaaay
                    });
                  }
                });
              }
            });
          });
        });
      }
  });
});

/* after login reload all data by user id. */
router.post('/reload_conversation', function(req, res, next) {
  models.instance.Users.find({}, {raw:true, allow_filtering: true}, function(err, users){
      if(err) throw err;
      //user is an array of plain objects with only name and age
      var alluserlist = [];
      var user = [];
      var msg = '';
      _.each(users, function(v,k){
        alluserlist.push({id: v.id, dept: v.dept, designation: v.designation, email: v.email, fullname: v.fullname, img: v.img});
      });
        
      var query = {
        participants: { $contains: user.id.toString() },
        group: { $eq: 'yes' },
        single: { $eq: 'no' }
      };
  
      models.instance.Conversation.find( query, { raw: true, allow_filtering: true }, function(err, conversations) {
        if (err) throw err;
        
        getActiveUsers((uresult, uerror) => {
          if (uerror) console.log(uerror);

          var blockUserListArray = [];
          var blockGroupListArray = [];

          if((uresult.users).length>0){
            _.each(uresult.users, function(users,k){
              if(users.id.toString() !== user.id.toString() ){
                blockUserListArray.push({
                  userid: users.id,
                  conversation_id:"",
                  conversation_type: "personal",
                  users_name: users.fullname,
                  users_img: users.img
                });
              }
            });
          }

          if((conversations).length>0){
            _.each(conversations, function(group,k){
              blockGroupListArray.push({
                userid: user.id.toString(),
                conversation_id: group.conversation_id,
                conversation_type: "group",
                users_name: group.title,
                users_img: 'feelix.jpg'
              });
            });
          }

          var finalListArray = blockUserListArray.concat(blockGroupListArray);
          
          getPersonalConversation(user.id.toString(), (cnvRes,cnvErr)=>{
            if(cnvErr){
              throw cnvErr;
            }else{
              
              var unreadUnpinArray = [];

              _.each(cnvRes.conversations, function(value,key){
                _.each(value.participants, function(v,k){
                  if(v != user.id.toString()){
                    _.each(finalListArray, function(vf,kf){
                      if(vf.userid == v){
                        vf.conversation_id = value.conversation_id.toString();
                        unreadUnpinArray.push(value.conversation_id.toString());
                      }
                    });
                  }
                });
              });

              getAllUnread(user.id.toString(), (urRes, urErr)=>{
                if(urErr){
                  throw urErr;
                }else{
    
                  var splitunpinnd =[];
                  var mdataunpinned =[];
                  var readMsgArr =[];
                  var readMsgArrCount =[];
    
                  var msgbodyArray = _.orderBy(urRes.unreadmsgbody, ['created_at'], ['desc']);
                  var ReadmsgbodyArray = _.orderBy(urRes.Readmsgbody, ['created_at'], ['desc']);
                  
                  _.each(msgbodyArray, function(value){
                    if(mdataunpinned.indexOf(value.conversation_id.toString()) == -1){
                      splitunpinnd.push(value);
                      mdataunpinned.push(value.conversation_id.toString());
                    }else{
                      mdataunpinned.push(value.conversation_id.toString());
                    }
                  });

                  _.each(ReadmsgbodyArray, function(value){
                    if(mdataunpinned.indexOf(value.conversation_id.toString()) == -1){
                      readMsgArr.push(value);
                      mdataunpinned.push(value.conversation_id.toString());
                    }
                  });

                  
                  var counts = {};
                  var newBody = [];
                  var myUserList = [];
                  var newBodyRead = [];
                  var myConversationList = urRes.unique;
                  
                  mdataunpinned.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });

                  _.each(finalListArray, function(value){
                    if(myConversationList.indexOf(value.conversation_id.toString()) !== -1){
                      myUserList.push(value);
                    }
                  });

                  _.each(readMsgArr, function(v,k){
                    newBodyRead.push({
                      'conversation_id':v.conversation_id,
                      'msg_body':v.msg_body,
                      'created_at':v.created_at,
                      'msg_id':v.msg_id,
                      'msg_type':v.msg_type,
                      'sender_img':v.sender_img,
                      'sender_name':v.sender_name,
                      'totalUnread':0
                    });
                  });

                  _.each(splitunpinnd, function(v,k){
                    newBody.push({
                      'conversation_id':v.conversation_id,
                      'msg_body':v.msg_body,
                      'created_at':v.created_at,
                      'msg_id':v.msg_id,
                      'msg_type':v.msg_type,
                      'sender_img':v.sender_img,
                      'sender_name':v.sender_name,
                      'totalUnread':counts[v.conversation_id.toString()]
                    });
                  });

                  var newBodyReadUnread = newBodyRead.concat(newBody);
                  var androidMsgList = [];
                  var totalmesgList = [];

                  _.each(myUserList, function(vm,k){
                    _.each(newBodyReadUnread, function(vs,k){
                      if(vm.conversation_id == vs.conversation_id){
                        androidMsgList.push(vs.conversation_id);
                        totalmesgList.push({
                          'conversation_id':vs.conversation_id,
                          'conversation_type':vm.conversation_type,
                          'conversation_title':vm.users_name,
                          'msg_body':vs.msg_body,
                          'created_at':vs.created_at,
                          'msg_id':vs.msg_id,
                          'msg_type':vs.msg_type,
                          'sender_img':vs.sender_img,
                          'sender_name':vs.sender_name,
                          'totalUnread': vs.totalUnread
                        });
                      }
                    });
                  });

                  _.each(myUserList, function(vm,k){
                    if(androidMsgList.indexOf(vm.conversation_id.toString()) == -1){
                      totalmesgList.push({
                        'conversation_id':vm.conversation_id,
                        'conversation_type':vm.conversation_type,
                        'conversation_title':vm.users_name,
                        'msg_body':"",
                        'created_at':moment().format('YYYY-MM-DDTHH:mm:ssZ'),
                        'msg_id':"",
                        'msg_type':"",
                        'sender_img':vm.users_img,
                        'sender_name':vm.users_name,
                        'totalUnread': 0
                      });
                    }
                  });

                  var RallMesgRaaay = _.orderBy(totalmesgList, ['created_at'], ['desc']);
                  
                  res.send({
                    alluserlist: alluserlist,
                    conversations:conversations, 
                    user: user, 
                    msg: msg, 
                    myUserList:RallMesgRaaay
                  });
                }
              });
            }
          });
        });
      });
  });
});

/* Send fcm . */
router.post('/fcm-send', function(req, res, next) {
  var sender_id = req.body.sender_id;
  var reciver_id = models.uuidFromString(req.body.reciver_id);
  var call_type = req.body.call_type;
  var msg = req.body.msg;

  models.instance.Users.find({id: reciver_id}, {raw:true, allow_filtering: true}, function(err, user){
      if(err) throw err;
      
      if(user[0].gcm_id != null){
        
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: user[0].gcm_id, 
            collapse_key: 'green',
            
            // notification: {
            //     title: 'Title of your push notification', 
            //     body: msg 
            // },
            
            data: {  //you can send only notification or only data(or include both)
                sender_id: sender_id,
                reciver_id: req.body.reciver_id,
                reciver_token: user[0].gcm_id,
                call_type: call_type,
                msg: msg
            }
        };
        
        fcm.send(message, function(err, response){
            if (err) {
                console.log(err);
            } else {
                res.send({status: "Successfully sent with response: ", response: response, message: message});
            }
        });
      }
      else{
        res.send({status: "user have no gcm id"});
      }
  });
});

// Url for add member ID in conversation tbl
router.post("/personalConCreate", function(req, res, next) {
  createPersonalConv( req.body.user_id, req.body.targetID, req.body.ecosystem, (result, err) =>{
    if (err) {
      if (err) throw err;
    } else if(result.status){
      // res.send(JSON.stringify(result));
      var conversation_id = result.conversation_id;
      findConversationHistory(conversation_id, (result, error) => {
        if(result.status){
          var conversation_list = _.sortBy(result.conversation, ["created_at"]);
          var unseenId = [];
          _.each(conversation_list, function(vc,kc){
            if(vc.msg_status == null){
              console.log(291,vc.msg_status);
              unseenId.push(vc.msg_id.toString());
            }else{
              var seenId = vc.msg_status;
              if(seenId.indexOf(req.body.user_id) == -1){
                unseenId.push(vc.msg_id.toString());
              }
            }
          });

          if(unseenId.length > 0){
            update_msg_status_add_viewer(unseenId, req.body.user_id, (result) =>{
              if(result.status){
                res.send({status: true, conversation_id: conversation_id, result: conversation_list});
              }else{
                console.log(result);
              }
            });
          }else{
            res.send({status: true, conversation_id: conversation_id, result: conversation_list});
          }
        }else{
          res.send({status: false, conversation_id: conversation_id, result: []});
        }
      });

    } else {
      res.send(false);
    }
  });
});

// All conversation history
router.post("/conversation_history", function(req, res, next) {
    var conversation_id = models.uuidFromString(req.body.conversation_id);
    findConversationHistory(conversation_id, (result, error) => {
      if(result.status){
        var conversation_list = _.sortBy(result.conversation, ["created_at"]);
        var unseenId = [];
        _.each(conversation_list, function(vc,kc){
          if(vc.msg_status == null){
            console.log(291,vc.msg_status);
            unseenId.push(vc.msg_id.toString());
          }else{
            var seenId = vc.msg_status;
            if(seenId.indexOf(req.body.user_id) == -1){
              unseenId.push(vc.msg_id.toString());
            }
          }
        });

        if(unseenId.length > 0){
          update_msg_status_add_viewer(unseenId, req.body.user_id, (result) =>{
            if(result.status){
              res.send({status: true, conversation_id: conversation_id, result: conversation_list});
            }else{
              console.log(result);
            }
          });
        }else{
          res.send({status: true, conversation_id: conversation_id, result: conversation_list});
        }
      }else{
        res.send({status: false, conversation_id: conversation_id, result: []});
      }
    });
});

// Delete a message
router.post('/commit_msg_delete', function(req, res, next){
    commit_msg_delete(req.body.msgid, req.body.uid, req.body.is_seen, req.body.remove, (result) =>{
      res.json(result);
    });
});

router.post('/flag_unflag', function(req, res, next){
  flag_unflag(req.body.msgid, req.body.uid, req.body.is_add, (result) =>{
    res.json(result);
  });
});

module.exports = router;
