var express = require('express');
var router = express.Router();
// var fileUpload = require('express-fileupload');
var multer = require('multer');
var highlight = require('highlight');
var moment = require('moment');
var path = require('path');
var _ = require('lodash');
var inArray = require('in-array');

var {models} = require('./../config/db/express-cassandra');
var {file2mimetype} = require('./../utils/mimetype');
var {getActiveUsers} = require('./../utils/chatuser');
var {saveConversation, findConversationHistory,checkAdmin,createPersonalConv,check_only_Creator_or_admin} = require('./../utils/conversation');
var {
  generateMessage,
  sendNewMsg,
  sendBusyMsg,
  commit_msg_delete,
  flag_unflag,
  add_reac_emoji,
  view_reac_emoji_list,
  get_group_lists,
  update_msg_status_add_viewer,
  check_reac_emoji_list,
  delete_reac_emoji,
  update_reac_emoji,
  get_messages_tag,
  getAllUnread,
  getPersonalConversation,
  replyId
} = require('./../utils/message');


// creates a configured middleware instance
// destination: handles destination
// filenane: allows you to set the name of the recorded file
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(`./public/upload/`))
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname.replace(path.extname(file.originalname), '@') +Date.now() +  path.extname(file.originalname));
    }
});

// utiliza a storage para configurar a instância do multer
const upload = multer({ storage });


/* GET listing. */
router.get("/", function(req, res, next) {
  if (req.session.login) {
    var query = {
      participants: { $contains: req.session.user_id },
      group: { $eq: 'yes' },
      single: { $eq: 'no' }
    };

    models.instance.Conversation.find(
      query,
      { raw: true, allow_filtering: true },
      function(err, peoples) {
        if (err) throw err;
        //people is an array of plain objects satisfying the query conditions above
        getActiveUsers((uresult, uerror) => {
          if (uerror) console.log(uerror);

          var blockUserListArray = [];
          var blockGroupListArray = [];
          var myid = [];

          if((uresult.users).length>0){
            _.each(uresult.users, function(user,k){
              if(user.id.toString() === req.session.user_id){
                myid.push({
                  userid: user.id,
                  conversation_id: user.id,
                  conversation_type: "personal",
                  box_type: 'default',
                  unread: 0,
                  // users_name: "You",
                  users_name: user.fullname,
                  users_img: user.img,
                  pined: true,
                  sub_title: user.designation,
                  last_msg: '',
                  last_msg_time:  user.createdat,
                  privecy:  'private',
                  totalMember : '2',
                  display: (alluserlist.indexOf(user.id.toString()) == -1 ? 'default':'success')
                });
              }else{
                blockUserListArray.push({
                  userid: user.id,
                  conversation_id: user.id,
                  conversation_type: "personal",
                  box_type: 'default',
                  unread: 0,
                  users_name: user.fullname,
                  users_img: user.img,
                  pined: false,
                  sub_title: user.designation,
                  last_msg: '',
                  last_msg_time:  user.createdat,
                  privecy:  'private',
                  totalMember : '1',
                  display: 'default'
                });
              }

            });
          }

          if((peoples).length>0){
            _.each(peoples, function(group,k){
              var totalMember = group.participants;
              blockGroupListArray.push({
                userid: req.session.user_id,
                conversation_id: group.conversation_id,
                conversation_type: "group",
                box_type: 'success',
                unread: 0,
                users_name: group.title,
                users_img: 'feelix.jpg',
                pined: false,
                sub_title: group.group_keyspace,
                last_msg: '',
                last_msg_time: group.created_at,
                privecy:  group.privacy,
                totalMember : totalMember.length,
                display: 'default'
              });
            });
          }


          var finalListArray = blockUserListArray.concat(blockGroupListArray);


          var sortedArray =[];
          var pin =[];
          var unpin =[];
          var personalCOnva = []

          getPersonalConversation(req.session.user_id, (cnvRes,cnvErr)=>{
            if(cnvErr){
              throw cnvErr;
            }else{


              var unreadUnpinArray = [];
              _.each(cnvRes.conversations, function(value,key){
                _.each(value.participants, function(v,k){
                  if(v != req.session.user_id){
                    _.each(finalListArray, function(vf,kf){
                      if(vf.userid == v){
                        vf.conversation_id = value.conversation_id.toString();
                        unreadUnpinArray.push(value.conversation_id.toString());
                      }
                    });
                  }
                });
              });

              models.instance.Pinned.find({user_id: models.uuidFromString(req.session.user_id) }, function(err, pinnedBlocks) {
                if (err) throw err;

                var totalPinned = parseInt(pinnedBlocks.length)+1;
                _.each(pinnedBlocks, function(val,key){
                  sortedArray.push(val.block_id.toString());
                });


                getAllUnread(req.session.user_id, (urRes, urErr)=>{
                  if(urErr){
                    throw urErr;
                  }else{

                    var uniqConv= urRes.unique;

                    _.each(finalListArray, function(value,key){
                      _.each(pinnedBlocks, function(val,key){
                        if( value.conversation_id.toString() == val.block_id.toString()){
                          sortedArray.push(val.block_id.toString());
                          //if(uniqConv.indexOf(value.conversation_id.toString()) !== -1){ //This line use for checking conversation has any message or not
                            pin.push({
                              pinned:val.id.toString(),
                              user_id: value.userid.toString(),
                              conversation_id: value.conversation_id.toString(),
                              conversation_type: value.conversation_type,
                              box_type: value.box_type,
                              unread: value.unread,
                              users_name: value.users_name,
                              users_img: value.users_img,
                              pined: true,
                              sub_title: value.sub_title,
                              last_msg: '',
                              last_msg_time: val.createdat,
                              privecy: value.privecy,
                              totalMember:value.totalMember,
                              display: 'default'
                            });
                          //}
                        }
                      });
                    });

                    _.each(finalListArray, function(value,key){
                      if(!inArray(sortedArray, value.conversation_id.toString() )){
                        if(uniqConv.indexOf(value.conversation_id.toString()) !== -1){
                          unpin.push({
                            user_id: value.userid.toString(),
                            conversation_id: value.conversation_id.toString(),
                            conversation_type: value.conversation_type,
                            box_type: value.box_type,
                            unread: value.unread,
                            users_name: value.users_name,
                            users_img: value.users_img,
                            pined: false,
                            sub_title: value.sub_title,
                            last_msg: '',
                            last_msg_time: key,
                            privecy: value.privecy,
                            totalMember:value.totalMember,
                            display: 'default'
                          });
                        }
                      }
                    });

                    var splitunpinnd = [];
                    var mdataunpinned = [];

                    var uniqueItems = Array.from(new Set(urRes.array_elements));

                    if(uniqueItems.length > 0){
                        _.each(unpin, function(value,unpinkey){
                          if(uniqueItems.indexOf(value.conversation_id) !== -1){
                            splitunpinnd.push(value);
                          }else{
                            mdataunpinned.push(value);
                          }

                        });

                        var splitunpinnd_desc = _.orderBy(splitunpinnd, ['last_msg_time'], ['desc']);
                        var mdataunpinned_desc = _.orderBy(mdataunpinned, ['last_msg_time'], ['desc']);

                        var explodObj = splitunpinnd_desc.concat(mdataunpinned_desc);
                    }else{
                        var explodObj = unpin;
                    }

                    var res_data = {
                      url:'hayven',
                      title: "Connect",
                      bodyClass: "chat",
                      success: req.session.success,
                      error: req.session.error,
                      user_id: req.session.user_id,
                      user_fullname: req.session.user_fullname,
                      user_email: req.session.user_email,
                      user_img: req.session.user_img,
                      highlight: highlight,
                      moment: moment,
                      _:_,
                      has_login: true,
                      data: [{
                        users: uresult.users,
                        groups: peoples,
                        finalListArray:finalListArray,
                        groupList:blockGroupListArray,
                        pin:pin,
                        myid:myid,
                        unpin:explodObj,
                        urRes:urRes,
                        cnvRes:cnvRes
                      }],
                    };
                    res.render("basic_view_connect", res_data);
                  }
                });
              });
            }
          });
        });
      }
    );

  } else {
    res.redirect("/");
  }
});


router.get("/chat/:type/:id/:conversationid/:name/:img", function(req,res,next) {
  if (req.session.login) {
    models.instance.Conversation.find({conversation_id: models.timeuuidFromString(req.params.conversationid) }, function(err, conversationDetail) {
        if (err) throw err;

        findConversationHistory(models.timeuuidFromString(req.params.conversationid), (result, error) => {
          var conversation_list = _.sortBy(result.conversation, ["created_at",]);

          get_group_lists(req.session.user_id, (groups, error_in_group) => {
            if(error_in_group)
              console.log(error_in_group);

            getActiveUsers((uresult, uerror) => {
              if(uerror)
                console.log(uerror);

                get_messages_tag(req.params.conversationid,(tagRes, tagError)=>{
                  if(tagError)
                    console.log(tagError);

                  console.log(tagRes.tags);
                  var res_data = {
                    url:'hayven',
                    title: "Connect",
                    bodyClass: "chat",
                    success: req.session.success,
                    error: req.session.error,
                    user_id: req.session.user_id,
                    conversationid: req.params.conversationid,
                    user_fullname: req.session.user_fullname,
                    user_email: req.session.user_email,
                    user_img: req.session.user_img,
                    to_user_name: req.params.name,
                    highlight: highlight,
                    _: _,
                    moment: moment,
                    file_message: "No",
                    has_login: true,

                    data: [
                      {
                        conversation_id: req.params.conversationid,
                        conversation_type: req.params.type,
                        users: uresult.users,
                        conversation: conversationDetail,
                        room_id: req.params.id,
                        room_name: req.params.name,
                        room_img: req.params.img,
                        conversation_list: conversation_list,
                        groups: groups.result,
                        tags: tagRes.tags
                      },
                    ],
                  };

                  res.render("open-chat", res_data);
                });
            });
          });
        });
    });
  } else {
    res.redirect("/");
  }
});

// For New Group Testing Purpose ocn = open chat test
router.get('/chat-t/:id/:name/:img', function(req, res, next){
  if(req.session.login){
    getActiveUsers((uresult, uerror) => {
      if(uerror) console.log(uerror);
        //user is an array of plain objects with only name and age
        var res_data = {
          url:'hayven',
          title: 'Connect',
          bodyClass: 'chat',
          success: req.session.success,
          error: req.session.error,
          user_id: req.session.user_id,
          user_fullname: req.session.user_fullname,
          user_email: req.session.user_email,
          user_img: req.session.user_img,
          highlight: highlight,
          moment: moment,
          file_message: 'No',
          has_login: true,
          data: [{'room_id':req.params.id, 'room_name':req.params.name, 'room_img':req.params.img,'users':uresult.users}] };
          res.render('oct', res_data);
    });
  } else {
    res.redirect('/');
  }
});

//This is a test route
router.get('/testmulter', function(req, res, next){
  res.render('textpage');
});

router.post('/send_message', upload.array('photos', 10), function(req, res, next){
  // res.json(req.files);
  // console.log(req.files);
  if(req.session.login){
    if (req.files.length < 1){
      res.json({'msg':'No files were uploaded.'});
    }
    else{
      res.json({'file_info': req.files, 'msg': 'Successfully uploaded'});
    }
  } else {
    res.redirect('/');
  }
});

router.post('/convImg', upload.single('photos'), function(req, res, next){
  if(req.session.login){
    res.json({'msg':'Successfully','filename':req.file.filename});
  } else {
    res.redirect('/');
  }
});

router.post('/msgFileUplod', upload.array('any_file_chat', 1000), function(req, res, next) {
    if (req.session.login) {
        if (req.files.length < 1) {
            res.json({ 'msg': 'No files were uploaded.' });
        } else {
            res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl });
        }
    } else {
        res.redirect('/');
    }
});

router.post('/open_thread', function(req, res, next) {
  if(req.session.login){
    replyId(req.body.msg_id, req.body.conversation_id, (result, err) =>{
      if(result.status){
        // console.log('hayven 377', _.toString(result.result));
        res.json(_.toString(result.result));
      } else {
        // console.log('hayven 380', err);
        res.json(result);
      }
    });
  }
});

router.post('/add_reac_emoji', function(req, res, next){
  if(req.session.login){
    check_reac_emoji_list(req.body.msgid, req.session.user_id, (result) =>{
      if(result.status){
        if(result.result.length == 0){
          // add first time like/reaction
          add_reac_emoji(req.body.msgid, req.session.user_id, req.session.user_fullname, req.body.emoji, (result1) =>{
            // console.log(290, result1);
            res.json(result1);
          });
        } else {
          if(result.result[0].emoji_name == req.body.emoji){
            // delete same user same type reaction
            delete_reac_emoji(req.body.msgid, req.session.user_id, req.body.emoji, (result2) =>{
              res.json(result2);
            });
          } else {
            update_reac_emoji(req.body.msgid, req.session.user_id, req.body.emoji, (result3) =>{
              res.json(result3);
            });
          }
        }
      }
    });
  } else {
    res.redirect('/');
  }
});
router.post('/emoji_rep_list', function(req, res, next){
  if(req.session.login){
    view_reac_emoji_list(req.body.msgid, req.body.emojiname, (result) =>{
      res.json(result.result);
    });
  } else {
    res.redirect('/');
  }
});

router.post('/flag_unflag', function(req, res, next){
  if(req.session.login){
    flag_unflag(req.body.msgid, req.body.uid, req.body.is_add, (result) =>{
      res.json(result);
    });
  } else {
    res.redirect('/');
  }
});


router.post('/commit_msg_delete', function(req, res, next){
  if(req.session.login){
    commit_msg_delete(req.body.msgid, req.body.uid, req.body.is_seen, req.body.remove, (result) =>{
      res.json(result);
    });
  } else {
    res.redirect('/');
  }
});

router.post('/update_msg_status', function(req, res, next){
  if(req.session.login){
    update_msg_status_add_viewer(JSON.parse(req.body.msgid_lists), req.body.user_id, (result) =>{
      res.json(result);
    });
  } else {
    res.redirect('/');
  }
});



router.get('/new-group', function(req, res, next){
  if(req.session.login){
    getActiveUsers((uresult, uerror) => {
      if(uerror) console.log(uerror);
        //user is an array of plain objects with only name and age
      var res_data = {
        url:'hayven',
        title: 'Connect',
        bodyClass: 'chat',
        success: req.session.success,
        error: req.session.error,
        user_id: req.session.user_id,
        user_fullname: req.session.user_fullname,
        user_email: req.session.user_email,
        user_img: req.session.user_img,
        has_login: true,
        data: [{'room_id':0, 'room_name':'Unnamed Group','users':uresult.users}] };
      res.render('chat-new-group', res_data);
    });

  } else {
    res.redirect('/');
  }
});


// Url for remove participants ID from conversation tbl
router.post("/groupMemberDelete", function(req, res, next) {
  if (req.session.login) {
    checkAdmin(req.body.conversation_id, req.body.targetID, result => {
      if (result) {
        if (result.status) {
          var newConversationArray = result.conversation;
          if (typeof newConversationArray[0] !== "undefined" && newConversationArray[0] !== null) {
            res.send(JSON.stringify("creator"));
          } else {
            models.instance.Conversation.update({conversation_id: models.timeuuidFromString(req.body.conversation_id),},{
                participants_admin: { $remove: [req.body.targetID] },
                participants: { $remove: [req.body.targetID] },
              },function(err) {
                if (err) {
                  if (err) throw err;
                } else {
                  res.send(JSON.stringify("success"));
                }
              }
            );
          }
        } else {
          console.log(result.status);
        }
      } else {
        console.log(result);
      }
    });
  } else {
    res.redirect("/");
  }
});

// Url for add participants ID in conversation tbl
router.post("/groupMemberAdd", function(req, res, next) {
  if (req.session.login) {
    models.instance.Conversation.update(
      { conversation_id: models.timeuuidFromString(req.body.conversation_id) },
      {
        participants: { $add: [req.body.targetID] },
      },
      function(err) {
        if (err) {
          if (err) throw err;
        } else {
          res.send(JSON.stringify("success"));
        }
      }
    );
  } else {
    res.redirect("/");
  }
});

// Url for leave from room
router.post("/leaveRoom", function(req, res, next) {
  if (req.session.login) {
    models.instance.Conversation.update(
      { conversation_id: models.timeuuidFromString(req.body.conversation_id) },
      {
        participants: { $remove: [req.body.targetID] },
      },
      function(err) {
        if (err) {
          if (err) throw err;
        } else {
          res.send(JSON.stringify("success"));
        }
      }
    );
  } else {
    res.redirect("/");
  }
});

// Url for add member ID in conversation tbl
router.post("/makeMember", function(req, res, next) {
  if (req.session.login) {
    models.instance.Conversation.update(
      { conversation_id: models.timeuuidFromString(req.body.conversation_id) },
      {
        participants_admin: { $remove: [req.body.targetID] },
      },
      function(err) {
        if (err) {
          if (err) throw err;
        } else {
          res.send(JSON.stringify("success"));
        }
      }
    );
  } else {
    res.redirect("/");
  }
});

// Url for add member ID in conversation tbl
router.post("/makeAdmin", function(req, res, next) {
  if (req.session.login) {
    models.instance.Conversation.update(
      { conversation_id: models.timeuuidFromString(req.body.conversation_id) },
      {
        participants_admin: { $add: [req.body.targetID] },
      },
      function(err) {
        if (err) {
          if (err) throw err;
        } else {
          res.send(JSON.stringify("success"));
        }
      }
    );
  } else {
    res.redirect("/");
  }
});


// Url for add member ID in conversation tbl
router.post("/personalConCreate", function(req, res, next) {
  if (req.session.login) {
    createPersonalConv( req.session.user_id, req.body.targetID, req.body.ecosystem, (result, err) =>{
		if (err) {
          if (err) throw err;
		} else if(result.status){
			res.send(JSON.stringify(result));
		} else {
			console.log(result);
		}
    });

    // res.json("hi");
  } else {
    res.redirect("/");
  }
});



// Pinned URL
router.post("/pinning", function(req, res, next) {
  if (req.session.login) {
    if(req.body.type == 'pin'){
      var id = models.uuid();
      var pinned = new models.instance.Pinned({
        id: id,
        user_id: models.uuidFromString(req.session.user_id),
        serial_number: parseInt(req.body.pinnedNumber),
        block_id:models.uuidFromString(req.body.blockID)
      });

      pinned.saveAsync().then(function() {
        res.send(JSON.stringify({status:true, pinID:id }));
      }).catch(function(err) {
        if (err) throw err;
      });

    }else if(req.body.type == 'unpin'){
      //DELETE FROM Pinned WHERE id='??';
      var query_object = {
        id: models.uuidFromString(req.body.targetID)
      };

      models.instance.Pinned.delete(query_object, function(err){
          if(err) res.send(JSON.stringify({err}));
          else {
            res.send(JSON.stringify({status:true }));
          }
      });
    }
  } else {
    res.redirect("/");
  }
});

// Url for delete conversation from conversation table by cpnversation id
router.post("/cnvDlt", function(req, res, next) {
  if (req.session.login) {

    check_only_Creator_or_admin(req.body.cnvID, req.body.targetID, result => {
      if(result.status){
        var query_object = {
          conversation_id: models.uuidFromString(req.body.cnvID)
        };
        models.instance.Conversation.delete(query_object, function(err){
            if(err) res.send(JSON.stringify({err}));
            else {
              res.send(JSON.stringify({msg:'success'}));
            }
        });
    }else{
      checkAdmin(req.body.cnvID, req.body.targetID, result => {
        if (result) {
          if (result.status) {
            var newConversationArray = result.conversation;
            if (typeof newConversationArray[0] !== "undefined" && newConversationArray[0] !== null) {
              var query_object = {
                conversation_id: models.uuidFromString(req.body.cnvID)
              };
              models.instance.Conversation.delete(query_object, function(err){
                  if(err) res.send(JSON.stringify({err}));
                  else {
                    res.send(JSON.stringify({msg:'success'}));
                  }
              });
            } else {
              res.send(JSON.stringify({result}));
            }
          } else {
            console.log(result.status);
          }
        } else {
          console.log(result);
        }
      });
    }


    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
